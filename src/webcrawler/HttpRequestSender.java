package webcrawler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import conf.NLPConstant;

/**
 * @author Yinzi Chen
 * @date Apr 13, 2014
 */
public class HttpRequestSender {

    // HTTP GET request
    public boolean sendGet(String url, String fileName) throws Exception {
        File file = new File(fileName);
        if (file.length() > 5000) {
            return false;
        }

        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");
        con.setRequestProperty("Cookie", NLPConstant.loginCookie);

        // add request header
        con.setRequestProperty("User-Agent", "Mozilla/5.0");

        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'GET' request to URL : " + url);
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(new InputStreamReader(
                con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        PrintWriter pw = new PrintWriter(new FileOutputStream(fileName));
        // print result
        pw.print(response.toString());
        pw.close();
        return true;

    }
}
