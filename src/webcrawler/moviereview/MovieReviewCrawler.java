package webcrawler.moviereview;

import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import model.MovieReview;
import webcrawler.HttpRequestSender;
import conf.GenreCategoryMapping;
import conf.NLPConstant;
import dao.MovieReviewDAO;

/**
 * @author Yinzi Chen
 * @date Apr 13, 2014
 */
public class MovieReviewCrawler {

    public void download() throws Exception {
        HttpRequestSender sender = new HttpRequestSender();
        GenreCategoryMapping mapping = new GenreCategoryMapping();
        mapping.initialize(NLPConstant.mappingFile);
        MovieReviewDAO dao = new MovieReviewDAO();
        dao.connect();
        int offset = 0;
        while (offset < 25380) {
            System.out.println("******************offset: " + offset
                    + "*****************");
            String url = NLPConstant.movieReviewBaseUrl + offset;
            String xmlFile = NLPConstant.cacheDir
                    + NLPConstant.getFilePath("movie_" + offset + ".xml",
                            NLPConstant.cacheDirNum);
            try {
                if (sender.sendGet(url, xmlFile)) {
                    Thread.sleep(150);
                }
            } catch (Exception e) {
                System.out
                        .println("Request failed, sleep for 5 secs and then retry.");
                Thread.sleep(5000);
                continue;
            }
            MovieReviewParser parser = new MovieReviewParser(xmlFile);
            List<MovieReview> reviewList = parser.getMovieReviewList();
            for (MovieReview review : reviewList) {
                // review.dump();
                String overviewFile = NLPConstant.overviewDir
                        + NLPConstant.getFilePath(
                                "overview_" + review.getMovieId() + ".xml",
                                NLPConstant.overviewDirNum);
                getPage(sender, review.getOverviewUrl(), overviewFile);

                if (new File(overviewFile).length() >= 5000) {
                    MovieReviewGeneralParser generalParser = new MovieReviewGeneralParser(
                            overviewFile);
                    review.setGenreStr(generalParser.getGenreStr());
                    review.setCountryStr(generalParser.getCountry());
                    List<String> genreList = review.getGenreList();
                    HashSet<String> categorySet = new HashSet<String>();
                    List<String> categoryList = new ArrayList<String>();
                    int[] vec = new int[11];
                    for (String genre : genreList) {
                        String categoryStr = mapping.getProperty(genre
                                .toLowerCase());
                        review.setCategoryStr(categoryStr);
                        for (String category : review.getCategoryList()) {
                            if (NLPConstant.categories.contains(category)) {
                                categorySet.add(category);
                                vec[NLPConstant.category2Idx.get(category)] = 1;
                            } else {
                                // categorySet.add("other");
                            }
                        }
                    }
                    for (String cat : categorySet) {
                        categoryList.add(cat);
                    }
                    String categoryVec = "" + vec[0];
                    for (int i = 1; i < 11; ++i) {
                        categoryVec += "," + vec[i];
                    }
                    review.setCategoryList(categoryList);
                    review.setCategoryVec(categoryVec);
                    // System.out.println(review.getOverviewUrl());
                    // System.out.println(review.getCountry());
                } else {
                    review.setGenreStr("");
                    review.setCountryStr("");
                    review.setCategoryStr("");
                    review.setCategoryVec("");
                }

                // String articleFile = NLPConstant.articleDir
                // + NLPConstant.getFilePath(
                // "article_" + review.getMovieId() + ".xml",
                // NLPConstant.articleDirNum);
                // getPage(sender, review.getArticleUrl(), articleFile);
                //
                // if (new File(articleFile).length() >= 5000) {
                // MovieReviewArticleParser articleParser = new
                // MovieReviewArticleParser(
                // articleFile);
                // // System.out.println(review.getArticleUrl());
                // review.setArticleTitle(articleParser.getArticleTitle());
                // review.setArticle(articleParser.getArticle());
                // } else {
                // review.setArticleTitle("");
                // review.setArticle("");
                // }

                // String detailFile = NLPConstant.detailDir
                // + NLPConstant.getFilePath(
                // "detail_" + review.getMovieId() + ".xml",
                // NLPConstant.detailDirNum);
                // getPage(sender, review.getDetailUrl(), detailFile);

                // String castFile = NLPConstant.castDir
                // + NLPConstant.getFilePath("cast_" + review.getMovieId()
                // + ".xml", NLPConstant.castDirNum);
                // getPage(sender, review.getCastUrl(), castFile);
                //
                // if (new File(castFile).length() >= 5000) {
                // MovieReviewCastParser castParser = new MovieReviewCastParser(
                // castFile);
                // // System.out.println(review.getArticleUrl());
                // List<String> cast2RoleList = castParser.getCastRoleList();
                // List<String> castList = new ArrayList<String>();
                // List<String> roleList = new ArrayList<String>();
                // HashSet<String> roleSet = new HashSet<String>();
                // for (String cast2Role : cast2RoleList) {
                // String[] str = cast2Role.split("-");
                // if (str.length != 2 || roleSet.contains(str[0].trim()))
                // continue;
                // roleSet.add(str[0]);
                // castList.add(str[0].trim());
                // roleList.add(str[1].trim());
                // }
                // review.setCastList(castList);
                // review.setRoleList(roleList);
                // // review.dump();
                // }

                try {
                    // dao.addMovieReview(review);
                    // dao.updateGenre(review);
                    // dao.updateCountry(review);
                    // dao.updateCategory(review);
                    // dao.updateCastRoleList(review);
                    // dao.updateImgUrl(review);
                    dao.updateCategoryVec(review);
                } catch (SQLException e) {
                    System.out.println(e.getMessage());
                }
            }
            offset += 20;
        }
        dao.close();
    }

    private void getPage(HttpRequestSender sender, String url, String fileName) {
        try {
            sender.sendGet(url, fileName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
