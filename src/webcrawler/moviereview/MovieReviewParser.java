package webcrawler.moviereview;

import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import model.MovieReview;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * @author Yinzi Chen
 * @date Apr 13, 2014
 */
public class MovieReviewParser {

    private Document doc;

    public MovieReviewParser(String xmlFile) throws Exception {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        doc = dBuilder.parse(xmlFile);
        doc.getDocumentElement().normalize();
    }

    public int getNumResults() {
        NodeList nList = doc.getElementsByTagName("num_results");
        return Integer.parseInt(nList.item(0).getTextContent());
    }

    public List<MovieReview> getMovieReviewList() {
        List<MovieReview> list = new ArrayList<MovieReview>();
        NodeList nList = doc.getElementsByTagName("review");
        for (int i = 0; i < nList.getLength(); ++i) {
            Element eElement = (Element) nList.item(i);
            MovieReview review = new MovieReview();
            review.setSourceId(1);
            review.setMovieId(Integer.parseInt(eElement
                    .getAttribute("nyt_movie_id")));
            review.setDisplayTitle(eElement
                    .getElementsByTagName("display_title").item(0)
                    .getTextContent());
            review.setPublicationDate(eElement
                    .getElementsByTagName("publication_date").item(0)
                    .getTextContent());
            if (eElement.getElementsByTagName("src").item(0) == null) {
                review.setImgUrl("");
            } else {
                review.setImgUrl(eElement.getElementsByTagName("src").item(0)
                        .getTextContent());
            }
            if (review.getPublicationDate().trim().equals("")) {
                review.setPublicationDate(eElement
                        .getElementsByTagName("opening_date").item(0)
                        .getTextContent());
            }
            NodeList links = eElement.getElementsByTagName("link");
            for (int j = 0; j < links.getLength(); ++j) {
                Element linkElement = (Element) links.item(j);
                String type = linkElement.getAttribute("type");
                String url = linkElement.getElementsByTagName("url").item(0)
                        .getTextContent();
                if (type.equals("article")) {
                    review.setArticleUrl(url);
                } else if (type.equals("overview")) {
                    review.setOverviewUrl(url);
                }
            }
            list.add(review);
        }
        return list;
    }
}
