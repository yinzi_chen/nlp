package webcrawler.moviereview;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

/**
 * @author Yinzi Chen
 * @date Apr 23, 2014
 */
public class MovieReviewCastParser {
    private Document doc;

    public MovieReviewCastParser(String xmlFile) throws Exception {
        doc = Jsoup.parse(new File(xmlFile), "UTF-8");
    }

    public List<String> getCastRoleList() {
        Elements links = doc.select("ul[class=credits flush]").select("li");
        List<String> castRoleList = new ArrayList<String>();
        for (int i = 0; i < links.size(); ++i) {
            String cast2Role = links.get(i).select("i").text().trim()
                    + links.get(i).text().trim();
            castRoleList.add(cast2Role);
        }
        return castRoleList;
    }

}
