package webcrawler.moviereview;

import java.io.File;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

/**
 * @author Yinzi Chen
 * @date Apr 20, 2014
 */
public class MovieReviewGeneralParser {
    private Document doc;

    public MovieReviewGeneralParser(String xmlFile) throws Exception {
        doc = Jsoup.parse(new File(xmlFile), "UTF-8");
    }

    public String getGenreStr() {
        Elements links = doc.select("span[itemprop=genre]");
        // if (!links.text().trim().equals(""))
        // return links.text();
        return links.text();
    }

    public String getCountry() {
        Elements links = doc.select("ul[class=meta flush]").select("li");
        // if (!links.text().trim().equals(""))
        // return links.text();
        for (int i = 0; i < links.size(); ++i) {
            String str = links.get(i).text();
            if (str.contains("Country:")) {
                return str.substring(str.indexOf("Country:") + 8).trim();
            }
        }
        return "";
    }
}
