package webcrawler.moviereview;

import java.io.File;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

/**
 * @author Yinzi Chen
 * @date Apr 13, 2014
 */
public class MovieReviewArticleParser {
    private Document doc;

    public MovieReviewArticleParser(String xmlFile) throws Exception {
        doc = Jsoup.parse(new File(xmlFile), "UTF-8");
    }

    public String getArticleTitle() {
        Elements links = doc.select("div[class=story-meta]").select(
                "h2[class=story-heading]");
        if (!links.text().trim().equals(""))
            return links.text();
        links = doc.select("div[id=movieTitle]").select(
                "h2[class=articleHeadline articleHeadlineSEO]");
        if (!links.text().trim().equals(""))
            return links.text();
        links = doc.select("div[id=article]").select(
                "h1[class=articleHeadline]");
        if (!links.text().trim().equals(""))
            return links.text();
        links = doc.select("div[id=article]").select("h1")
                .select("nyt_headline");
        if (!links.text().trim().equals(""))
            return links.text();
        links = doc.select("article[id=story]").select(
                "h1[class=story-heading]");
        if (!links.text().trim().equals(""))
            return links.text();
        links = doc.select("nyt_headline").select("h2");
        if (!links.text().trim().equals(""))
            return links.text();
        links = doc.select("div[id=articleBody]").select("h1");
        return links.get(1).text();
    }

    public String getArticle() {
        Elements links = doc.select("p[class=story-body-text story-content]");
        if (!links.text().trim().equals(""))
            return links.text();
        links = doc.select("div[class=articleBody]");
        if (!links.text().trim().equals(""))
            return links.text();
        links = doc.select("div[id=articleBody]").select("p");
        if (!links.text().trim().equals(""))
            return links.text();
        links = doc.select("nyt_text").select("p");
        return links.text();
    }
}
