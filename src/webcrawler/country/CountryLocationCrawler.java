package webcrawler.country;

import java.io.File;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import model.MovieCountryStat;
import model.MovieReview;
import webcrawler.HttpRequestSender;
import conf.NLPConstant;
import dao.MovieCountryStatDAO;
import dao.MovieReviewDAO;

/**
 * @author Yinzi Chen
 * @date Apr 26, 2014
 */
public class CountryLocationCrawler {

    public static void main(String[] args) throws Exception {
        HttpRequestSender sender = new HttpRequestSender();
        MovieReviewDAO dao = new MovieReviewDAO();
        MovieCountryStatDAO statDao = new MovieCountryStatDAO();
        dao.connect();
        statDao.connect();
        String begin = "1900-01-01";
        String end = "2014-12-31";
        List<MovieReview> reviewList = dao.getMovieReviewBasicInfoByDate(begin,
                end);
        System.out.println(reviewList.size());
        String baseUrl = NLPConstant.addressLocationBaseUrl;
        Map<String, MovieCountryStat> statMap = new HashMap<String, MovieCountryStat>();
        for (MovieReview review : reviewList) {
            List<String> countryList = review.getCountryList();
            List<String> categoryList = review.getCategoryList();
            for (String country : countryList) {
                country = country.toLowerCase();
                String url = baseUrl + URLEncoder.encode(country);
                String countryFile = NLPConstant.countryDir + "location_"
                        + country + ".xml";
                if (!new File(countryFile).exists()) {
                    sender.sendGet(url, countryFile);
                    Thread.sleep(300);
                }
                CountryLocationParser parser = new CountryLocationParser(
                        countryFile);
                MovieCountryStat stat = statMap.get(parser.getFormattedName());
                if (stat == null) {
                    stat = new MovieCountryStat(parser.getFormattedName(),
                            parser.getLatitude(), parser.getLongitude());
                    statMap.put(parser.getFormattedName(), stat);
                }
                for (String category : categoryList) {
                    if (category.equals("action & adventure")) {
                        stat.setActionCnt(stat.getActionCnt() + 1);
                    } else if (category.equals("comedy")) {
                        stat.setComedyCnt(stat.getComedyCnt() + 1);
                    } else if (category.equals("drama")) {
                        stat.setDramaCnt(stat.getDramaCnt() + 1);
                    } else if (category.equals("romance")) {
                        stat.setRomanceCnt(stat.getRomanceCnt() + 1);
                    } else if (category.equals("crime")) {
                        stat.setCrimeCnt(stat.getCrimeCnt() + 1);
                    } else if (category.equals("documentary & biography")) {
                        stat.setDocumentaryCnt(stat.getDocumentaryCnt() + 1);
                    } else if (category.equals("mystery & suspense")) {
                        stat.setSuspenseCnt(stat.getSuspenseCnt() + 1);
                    } else if (category.equals("science fiction & fantasy")) {
                        stat.setFantasyCnt(stat.getFantasyCnt() + 1);
                    } else if (category.equals("art")) {
                        stat.setArtCnt(stat.getArtCnt() + 1);
                    } else if (category.equals("animation & cartoons")) {
                        stat.setAnimationCnt(stat.getAnimationCnt() + 1);
                    } else if (category.equals("horror")) {
                        stat.setHorrorCnt(stat.getHorrorCnt() + 1);
                    } else {
                        System.out.println("*********error********");
                    }
                }
            }
        }
        for (MovieCountryStat stat : statMap.values()) {
            statDao.addMovieCountryStat(stat);
        }
        statDao.close();
        dao.close();
    }
}
