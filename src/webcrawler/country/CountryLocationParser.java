package webcrawler.country;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

/**
 * @author Yinzi Chen
 * @date Apr 26, 2014
 */
public class CountryLocationParser {

    private Document doc;

    public CountryLocationParser(String xmlFile) throws Exception {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        doc = dBuilder.parse(xmlFile);
        doc.getDocumentElement().normalize();
    }

    public String getFormattedName() {
        NodeList nList = doc.getElementsByTagName("formatted_address");
        return nList.item(0).getTextContent();
    }

    public float getLatitude() {
        String lat = doc.getElementsByTagName("lat").item(0).getTextContent();
        return Float.parseFloat(lat);
    }

    public float getLongitude() {
        String lng = doc.getElementsByTagName("lng").item(0).getTextContent();
        return Float.parseFloat(lng);
    }
}
