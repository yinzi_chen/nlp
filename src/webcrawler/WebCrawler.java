package webcrawler;

import webcrawler.moviereview.MovieReviewCrawler;

/**
 * @author Yinzi Chen
 * @date Apr 13, 2014
 */
public class WebCrawler {

    public static void main(String[] args) throws Exception {
        MovieReviewCrawler crawler = new MovieReviewCrawler();
        crawler.download();
    }

}
