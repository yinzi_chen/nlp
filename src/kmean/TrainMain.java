package kmean;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class TrainMain {
	public static void main(String[] args) throws Exception {
		List<movie> listMovies=new ArrayList<movie>();		
		@SuppressWarnings("resource")
		BufferedReader br = new BufferedReader(new FileReader(config.tfilename));
		String line = br.readLine();
		int id =0;
		while( line!=null){
			String[] sdata = line.split(" ");
			movie p1=new movie();
			p1.setid(id);
			id=(int) (id+5*Math.random());
			int[] data = new int[config.propertynumber];
			for (int i=0; i< config.propertynumber; i++)
			{
				data[i]=Integer.parseInt(sdata[i]);
			}
			p1.set6number(data);
	        listMovies.add(p1);  			
			line = br.readLine();
		}
		kmean k = new kmean();
		k.init();
		k.train(listMovies);
	}
}
