package kmean;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

class Cluster { 
	int id; 
    int movieCount; 
    List<movie> movies;
    int[] centers;
    float[] center; 
    public void clear() { 
    	centers = new int[config.propertynumber];
        center = new float[config.propertynumber];     	
    	for(int i =0; i<config.propertynumber; i++)
        {
        	centers[i] = 0;
        	center[i] = 0;
        }
    	movieCount =0;
    	movies = new ArrayList<movie>();
    } 
    public Cluster(int id, movie movie) { 
        this.id = id; 
        clear();
        addmovie(movie); 
    }
    public Cluster() { 
         
    }
    
    int getId() { 
        return id; 
    } 
     
    float[] getcenter() { 
        return this.center; 
    } 
    
    void addmovie(movie movie) { 
        movies.add(movie);
    	movieCount++;
    	int[] temp = movie.get6number();
        for(int i =0; i<config.propertynumber; i++)
        {
        	centers[i] += temp[i];
        	center[i] = (float)centers[i]/(float)movieCount;
        }
    } 
    void savedata() throws FileNotFoundException { 
        String filename = "./src/kmean/cluster"+id+".txt";
    	PrintWriter pw = new PrintWriter(new OutputStreamWriter(new FileOutputStream(filename)),true);  
    	for(int i = 0; i<config.propertynumber; i++)
        {
    		pw.print(this.center[i]+" ");
        }
    	pw.println();
    	pw.println(this.movieCount);
    	for(movie k: movies)
        {
    		pw.print(k.id + "%");
    		int[] tmp = k.get6number();
    		for (int i: tmp)
    		{
    			pw.print (i +" ");
    		}
    		pw.println();
        }
    	pw.close();
    } 
    public List<movie> showrecom(){ 
        return movies;
    } 
    
    void removemovie(movie movie) { 
    	movieCount--;
    	movies.remove(movie);
    	int[] temp = movie.get6number();
        for(int i =0; i<config.propertynumber; i++)
        {
        	centers[i] -= temp[i];
        	center[i] = (float)centers[i]/(float)movieCount;
        } 
    }
   
    float distance(movie movie) { 
    	float temp = 0;;
    	int[] md = movie.get6number();
        for(int i =0; i<config.propertynumber; i++)
        {
        	temp += Math.abs(center[i]-(float)md[i]);
    	}
    	return temp/config.propertynumber; 
    } 
} 
