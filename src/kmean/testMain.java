package kmean;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class testMain {
	public static Cluster[] clusters;
	public static void main(String[] args) throws Exception {
		clusters = new Cluster[config.clusternumber];
		for(int i =0; i<config.clusternumber; i++)
		{
			clusters[i]=readcluster(i);
		}
		movie p1=new movie();
		int[] n = {50,10,10,5,2,9};
		int[] n1 = {0,10,70,5,2,9};
		int[] n2 = {50,10,10,59,2,9};
		int[] n3 = {2,10,10,5,2,99};				
		p1.set6number(n3);
		Cluster thiscluster = findcluster(p1);
		System.out.println("cluster is " + thiscluster.id);
		List<movie> Ms= thiscluster.showrecom();
		for(movie k : Ms)
		{
			System.out.println("movie id " + k.id);
		}		
	}
	public static Cluster readcluster(int clusterid) throws Exception
	{
		String filename = "./src/kmean/cluster"+clusterid+".txt";
		@SuppressWarnings("resource")
		BufferedReader br = new BufferedReader(new FileReader(filename));
				
		String line = br.readLine();
		String[] sdata = line.split(" ");
		Cluster c = new Cluster();
		c.clear();
		c.id = clusterid;
		for (int i=0; i< config.propertynumber; i++)
		{
			c.center[i]=Float.parseFloat(sdata[i]);
		}
		line = br.readLine();
		c.movieCount = Integer.parseInt(line);
		line = br.readLine();
	
		while( line!=null){
			sdata = line.split("%");			
			movie p1=new movie();
			p1.setid(Integer.parseInt(sdata[0]));
			c.movies.add(p1);
			line = br.readLine();
			
		}		
		return c;		
	}
	public static Cluster findcluster(movie movie) throws Exception
	{
		float minidistance = 10000;
		int whichcluster = 7;
		for(int i =0; i<config.clusternumber; i++)
        {
			float tmp = clusters[i].distance(movie);
			
			if (minidistance > tmp)
			{
				whichcluster = i;
				minidistance = tmp;
			}
        }
		return clusters[whichcluster];
	}	
}
