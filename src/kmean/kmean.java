package kmean;

import java.io.FileNotFoundException;
import java.util.List;

public class kmean {
	static Cluster[] clusters;     	
	public void init(){
		clusters = new Cluster[config.clusternumber];
	}
	public void train(List<movie> listmovie) throws FileNotFoundException{
		// create initial clusters 
		for (int i=0;i<config.clusternumber;i++) { 
        	clusters[i] = new Cluster(i,listmovie.get(i)); 
        }
		// starting training data
		int count = config.clusternumber;
		while (count < config.totalmovies){
			movie nextmovie = listmovie.get(count);
			count++;
			float minidistance = 10000;
			int whichcluster = 7;
			for(int i =0; i<config.clusternumber; i++)
	        {
				float tmp = clusters[i].distance(nextmovie);
				
				if (minidistance > tmp)
				{
					whichcluster = i;
					minidistance = tmp;
				}
	        }
			clusters[whichcluster].addmovie(nextmovie);
		}
		for (int i=0;i<config.clusternumber;i++) { 
        	clusters[i].savedata(); 
        }
		
	}
	
}
