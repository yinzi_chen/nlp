package kmean;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

public class fkdata {
	public static void makedata() throws FileNotFoundException
	{
		PrintWriter pw = new PrintWriter(new OutputStreamWriter(new FileOutputStream(config.fkfilename)),true);  
        int count = 0;
		while (count < config.totalmovies)
        {
        	for (int h = 0; h < config.clusternumber; h++)
        	{
        		float offset = config.maximumvalue*h/config.clusternumber;        		
	        	for (int j = 0; j < config.propertynumber; j++)
	        	{
	        		pw.print((int)(Math.random()*config.maximumvalue/config.clusternumber + offset) + " ");	        		
	        	}
	        	count++;
	        	pw.println();
        	}
        }
        pw.close();
	}
	public static void makedata2() throws FileNotFoundException
	{
		PrintWriter pw = new PrintWriter(new OutputStreamWriter(new FileOutputStream(config.fkfilename)),true);  
        int count = 0;
		while (count < config.totalmovies)
        {
        	for (int h = 0; h < config.clusternumber; h++)
        	{
	        	for (int j = 0; j < config.propertynumber; j++)
	        	{
	        		int tt = h%config.propertynumber;
	        		if (j==tt)
	        		{
	        			pw.print((int)((float)Math.random()*config.maximumvalue) + " ");	        		
	        		}
	        		else {
	        			pw.print((int)((float)Math.random()*2) + " ");	        		
	        		}
	        	}
	        	count++;
	        	pw.println();
        	}
        }
        pw.close();
	}
	public static void makedata3() throws FileNotFoundException
	{
		PrintWriter pw = new PrintWriter(new OutputStreamWriter(new FileOutputStream(config.fkfilename)),true);  
        int count = 0;
		while (count < config.totalmovies)
        {
        	for (int j = 0; j < config.propertynumber; j++)
        	{
        		pw.print((int)(Math.random()*config.maximumvalue) + " ");	        		
        	}
        	count++;
        	pw.println();
        }
        pw.close();
	}
	public static void main(String[] args) throws Exception {
		makedata2();
	}
}
