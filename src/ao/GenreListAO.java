package ao;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import model.MovieReview;
import dao.MovieReviewDAO;

/**
 * @author Yinzi Chen
 * @date Apr 21, 2014
 */
public class GenreListAO {

    public static void main(String[] args) throws Exception {
        MovieReviewDAO dao = new MovieReviewDAO();
        dao.connect();
        String begin = "1900-01-01";
        String end = "2014-12-31";
        Set<String> set = new HashSet<String>();
        List<MovieReview> reviewList = dao.getMovieReviewBasicInfoByDate(begin,
                end);
        for (MovieReview review : reviewList) {
            List<String> genreList = review.getGenreList();
            for (String genre : genreList) {
                set.add(genre);
            }
        }
        System.out.println("There are totally " + set.size()
                + " different genres.");
        for (String genre : set) {
            System.out.println(genre);
        }
        dao.close();
    }

}
