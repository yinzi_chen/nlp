package ao;

import java.util.List;
import java.util.Random;

import dao.MovieEventDao;

/**
 * @author Yinzi Chen
 * @date Apr 29, 2014
 */
public class UpdateCategoryVector {

    public static void main(String[] args) throws Exception {
        MovieEventDao dao = new MovieEventDao();
        dao.connect();
        List<String[]> res = dao.getNameAndCategory();
        Random r = new Random();
        for (String[] str : res) {
            String[] vc = str[1].split(",");
            float[] val = new float[vc.length];
            for (int i = 0; i < vc.length; ++i) {
                if (vc[i].equals("0")) {
                    val[i] = r.nextFloat() * 0.3f;
                } else {
                    val[i] = r.nextFloat() * 0.3f + 0.7f;
                }
            }
            String category = "" + val[0];
            for (int i = 1; i < val.length; ++i) {
                category += "," + val[i];
            }
            // System.out.println(str[1] + " " + category);
            dao.updateCategory(str[0], category);
        }
        dao.close();
    }

}
