package ao;

import java.util.List;

import model.MovieReview;
import dao.MovieReviewDAO;

/**
 * @author Yinzi Chen
 * @date Apr 15, 2014
 */
public class TestAO {

    public static void main(String[] args) throws Exception {
        MovieReviewDAO dao = new MovieReviewDAO();
        dao.connect();
        List<MovieReview> reviewList = dao
                .getMovieReviewByDate("2013-12-25", 0);
        for (MovieReview review : reviewList) {
            review.dump();
        }
        int cnt1 = dao.getMovieReviewCountByDate("2013-12-25");
        System.out.println(cnt1);
        int cnt2 = dao.getMovieReviewCountByDate("1900-01-01", "2014-12-31");
        System.out.println(cnt2);
        dao.close();
    }

}
