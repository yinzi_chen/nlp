package ao.example;

import java.util.Map;

import conf.NLPConstant;

import dao.ActorInfoDAO;

/**
 * @author Yinzi Chen
 * @date Apr 25, 2014
 */
public class ActorInfoExample {

    public static void main(String[] args) throws Exception {
        ActorInfoDAO dao = new ActorInfoDAO();
        Map<String, Integer> map = dao.getActorInfo("Tom Cruise");

        dao = new ActorInfoDAO(NLPConstant.castTop6File);
        map = dao.getActorInfo("Tom Cruise");

        dao = new ActorInfoDAO(NLPConstant.castTop10File);
        map = dao.getActorInfo("Tom Cruise");

        dao = new ActorInfoDAO(NLPConstant.castAllFile);// recommend to use this
                                                        // complete data file
        map = dao.getActorInfo("Tom Cruise");
    }

}
