package ao;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import model.MovieReview;
import dao.MovieReviewDAO;

/**
 * @author Yinzi Chen
 * @date Apr 21, 2014
 */
public class CountryListAO {

    public static void main(String[] args) throws Exception {
        MovieReviewDAO dao = new MovieReviewDAO();
        dao.connect();
        String begin = "1900-01-01";
        String end = "2014-12-31";
        Set<String> set = new HashSet<String>();
        List<MovieReview> reviewList = dao.getMovieReviewBasicInfoByDate(begin,
                end);
        for (MovieReview review : reviewList) {
            List<String> countryList = review.getCountryList();
            for (String country : countryList) {
                set.add(country);
            }
        }
        System.out.println("There are totally " + set.size()
                + " different countries.");
        for (String country : set) {
            System.out.println(country);
        }
        dao.close();
    }

}
