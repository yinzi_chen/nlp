package ao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import model.MovieReview;
import tools.SaveObjects;
import conf.NLPConstant;
import dao.MovieReviewDAO;

/**
 * @author Yinzi Chen
 * @date Apr 24, 2014
 */
public class CastListAO {
    static Map<String, Map<String, Integer>> map;

    public static void main(String[] args) throws Exception {
        loadCastData(10000, NLPConstant.castAllFile);
        loadCastData(4, NLPConstant.castTop4File);
        loadCastData(6, NLPConstant.castTop6File);
        loadCastData(10, NLPConstant.castTop10File);
        // ActorInfoDAO dao = new ActorInfoDAO();
        // Map<String, Integer> map = dao.getActorInfo("Tom Cruise");
    }

    public static Map<String, Map<String, Integer>> loadCastData(int topk,
            String file) throws Exception {
        MovieReviewDAO dao = new MovieReviewDAO();
        dao.connect();
        String begin = "1900-01-01";
        String end = "2014-12-31";
        Map<String, Map<String, Integer>> map = new HashMap<String, Map<String, Integer>>();
        List<MovieReview> reviewList = dao.getMovieReviewBasicInfoByDate(begin,
                end);
        for (MovieReview review : reviewList) {
            List<String> categoryList = review.getCategoryList();
            List<String> castList = review.getCastList();
            for (int i = 0; i < Math.min(topk, castList.size()); ++i) {
                String name = castList.get(i).trim();
                Map<String, Integer> mmap = map.get(name);
                if (mmap == null) {
                    mmap = new HashMap<String, Integer>();
                    map.put(name, mmap);
                }
                for (String category : categoryList) {
                    Integer num = mmap.get(category);
                    if (num == null) {
                        num = 0;
                    }
                    mmap.put(category, num + 1);
                }
            }
        }
        SaveObjects.save(map, file);
        System.out.println("There are totally " + map.size()
                + " different actors.");
        // for (String category : set) {
        // System.out.println(category);
        // }
        dao.close();
        return map;
    }

}
