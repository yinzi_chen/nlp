package model;

import java.io.Serializable;

/**
 * @author Yinzi Chen
 * @date Apr 26, 2014
 */
public class MovieCountryStat implements Serializable {

    private static final long serialVersionUID = 1L;
    private String countryName;
    private float latitude;
    private float longitude;
    private int actionCnt;
    private int romanceCnt;
    private int suspenseCnt;
    private int comedyCnt;
    private int artCnt;
    private int crimeCnt;
    private int documentaryCnt;
    private int fantasyCnt;
    private int dramaCnt;
    private int horrorCnt;
    private int animationCnt;

    public MovieCountryStat(String name, float lat, float lng) {
        countryName = name;
        latitude = lat;
        longitude = lng;
        setRomanceCnt(0);
        setSuspenseCnt(0);
        setCrimeCnt(0);
        setDocumentaryCnt(0);
        setAnimationCnt(0);
        actionCnt = 0;
        comedyCnt = 0;
        artCnt = 0;
        fantasyCnt = 0;
        dramaCnt = 0;
        horrorCnt = 0;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public int getActionCnt() {
        return actionCnt;
    }

    public void setActionCnt(int actionCnt) {
        this.actionCnt = actionCnt;
    }

    public int getComedyCnt() {
        return comedyCnt;
    }

    public void setComedyCnt(int comedyCnt) {
        this.comedyCnt = comedyCnt;
    }

    public int getArtCnt() {
        return artCnt;
    }

    public void setArtCnt(int artCnt) {
        this.artCnt = artCnt;
    }

    public int getFantasyCnt() {
        return fantasyCnt;
    }

    public void setFantasyCnt(int fantasyCnt) {
        this.fantasyCnt = fantasyCnt;
    }

    public int getDramaCnt() {
        return dramaCnt;
    }

    public void setDramaCnt(int dramaCnt) {
        this.dramaCnt = dramaCnt;
    }

    public int getHorrorCnt() {
        return horrorCnt;
    }

    public void setHorrorCnt(int horrorCnt) {
        this.horrorCnt = horrorCnt;
    }

    public int getRomanceCnt() {
        return romanceCnt;
    }

    public void setRomanceCnt(int romanceCnt) {
        this.romanceCnt = romanceCnt;
    }

    public int getSuspenseCnt() {
        return suspenseCnt;
    }

    public void setSuspenseCnt(int suspenseCnt) {
        this.suspenseCnt = suspenseCnt;
    }

    public int getCrimeCnt() {
        return crimeCnt;
    }

    public void setCrimeCnt(int crimeCnt) {
        this.crimeCnt = crimeCnt;
    }

    public int getDocumentaryCnt() {
        return documentaryCnt;
    }

    public void setDocumentaryCnt(int documentaryCnt) {
        this.documentaryCnt = documentaryCnt;
    }

    public int getAnimationCnt() {
        return animationCnt;
    }

    public void setAnimationCnt(int animationCnt) {
        this.animationCnt = animationCnt;
    }

}
