package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Yinzi Chen
 * @date Apr 13, 2014
 */
public class MovieReview implements Serializable {
    private static final long serialVersionUID = 1L;
    private int sourceId;
    private int movieId;
    private String displayTitle;
    private String publicationDate;
    private String imgUrl;
    private String articleUrl;
    private String overviewUrl;
    private String articleTitle;
    private String article;
    private List<String> countryList;
    private List<String> genreList;
    private List<String> categoryList;
    private List<String> castList;
    private List<String> roleList;
    private String categoryVec;

    public MovieReview() {
        genreList = new ArrayList<String>();
        countryList = new ArrayList<String>();
        categoryList = new ArrayList<String>();
        castList = new ArrayList<String>();
        roleList = new ArrayList<String>();
    }

    public void dump() {
        System.out.println("============Dumping movie review============");
        System.out.println("sourceId: " + sourceId);
        System.out.println("movieId: " + movieId);
        System.out.println("displayTitle: " + displayTitle);
        System.out.println("publicationDate: " + publicationDate);
        System.out.println("imgUrl: " + imgUrl);
        System.out.println("articleUrl: " + articleUrl);
        System.out.println("overviewUrl: " + overviewUrl);
        System.out.println("articleTitle: " + articleTitle);
        System.out.println("countryList: " + getCountryStr());
        System.out.println("genreList: " + getGenreStr());
        System.out.println("categoryList: " + getCategoryStr());
        System.out.println("castList: " + getCastStr() + " (" + castList.size()
                + ")");
        System.out.println("roleList: " + getRoleStr() + " (" + roleList.size()
                + ")");
        System.out.println("article: " + article);
        System.out.println("====================END=====================");
    }

    public String getDisplayTitle() {
        return displayTitle;
    }

    public void setDisplayTitle(String displayTitle) {
        this.displayTitle = displayTitle;
    }

    public String getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(String publicationDate) {
        this.publicationDate = publicationDate;
    }

    public String getArticleUrl() {
        return articleUrl;
    }

    public void setArticleUrl(String articleUrl) {
        this.articleUrl = articleUrl;
    }

    public String getOverviewUrl() {
        return overviewUrl;
    }

    public String getDetailUrl() {
        int idx = getOverviewUrl().lastIndexOf('/');
        if (idx == -1)
            return "";
        String detailUrl = getOverviewUrl().substring(0, idx + 1);
        return detailUrl + "details";
    }

    public String getCastUrl() {
        int idx = getOverviewUrl().lastIndexOf('/');
        if (idx == -1)
            return "";
        String detailUrl = getOverviewUrl().substring(0, idx + 1);
        return detailUrl + "cast";
    }

    public void setOverviewUrl(String overviewUrl) {
        this.overviewUrl = overviewUrl;
    }

    public String getArticle() {
        return article;
    }

    public void setArticle(String article) {
        this.article = article;
    }

    public List<String> getGenreList() {
        return genreList;
    }

    public void setGenreList(List<String> genreList) {
        this.genreList = genreList;
    }

    public int getSourceId() {
        return sourceId;
    }

    public void setSourceId(int sourceId) {
        this.sourceId = sourceId;
    }

    public int getMovieId() {
        return movieId;
    }

    public void setMovieId(int movieId) {
        this.movieId = movieId;
    }

    public String getArticleTitle() {
        return articleTitle;
    }

    public void setArticleTitle(String articleTitle) {
        this.articleTitle = articleTitle;
    }

    public String getGenreStr() {
        if (genreList.size() == 0)
            return "";
        String genreStr = genreList.get(0);
        for (int i = 1; i < genreList.size(); ++i) {
            genreStr += "," + genreList.get(i);
        }
        return genreStr;
    }

    public void setGenreStr(String genreStr) {
        genreList.clear();
        if (genreStr == null)
            return;
        String[] genres = genreStr.split(",");
        for (String genre : genres) {
            genreList.add(genre.trim());
        }
    }

    public List<String> getCountryList() {
        return countryList;
    }

    public void setCountryList(List<String> countryList) {
        this.countryList = countryList;
    }

    public String getCountryStr() {
        if (countryList.size() == 0)
            return "";
        String countryStr = countryList.get(0);
        for (int i = 1; i < countryList.size(); ++i) {
            countryStr += "," + countryList.get(i);
        }
        return countryStr;
    }

    public void setCountryStr(String countryStr) {
        countryList.clear();
        if (countryStr == null || countryStr.trim().equals(""))
            return;
        String[] countries = countryStr.split(",");
        for (String country : countries) {
            countryList.add(country.trim());
        }
    }

    public List<String> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<String> categoryList) {
        this.categoryList = categoryList;
    }

    public String getCategoryStr() {
        if (categoryList.size() == 0)
            return "";
        String categoryStr = categoryList.get(0);
        for (int i = 1; i < categoryList.size(); ++i) {
            categoryStr += "," + categoryList.get(i);
        }
        return categoryStr;
    }

    public void setCategoryStr(String categoryStr) {
        categoryList.clear();
        if (categoryStr == null || categoryStr.trim().equals(""))
            return;
        String[] categories = categoryStr.split(",");
        for (String category : categories) {
            categoryList.add(category.trim());
        }
    }

    public List<String> getCastList() {
        return castList;
    }

    public void setCastList(List<String> castList) {
        this.castList = castList;
    }

    public List<String> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<String> roleList) {
        this.roleList = roleList;
    }

    public String getCastStr() {
        if (castList.size() == 0)
            return "";
        String castStr = castList.get(0);
        for (int i = 1; i < castList.size(); ++i) {
            castStr += "," + castList.get(i);
        }
        return castStr;
    }

    public String getRoleStr() {
        if (roleList.size() == 0)
            return "";
        String roleStr = roleList.get(0);
        for (int i = 1; i < roleList.size(); ++i) {
            roleStr += "," + roleList.get(i);
        }
        return roleStr;
    }

    public void setCastStr(String castStr) {
        castList.clear();
        if (castStr == null || castStr.trim().equals(""))
            return;
        String[] casts = castStr.split(",");
        for (String cast : casts) {
            castList.add(cast.trim());
        }
    }

    public void setRoleStr(String roleStr) {
        roleList.clear();
        if (roleStr == null || roleStr.trim().equals(""))
            return;
        String[] roles = roleStr.split(",");
        for (String role : roles) {
            roleList.add(role.trim());
        }
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getCategoryVec() {
        return categoryVec;
    }

    public void setCategoryVec(String categoryVec) {
        this.categoryVec = categoryVec;
    }

}
