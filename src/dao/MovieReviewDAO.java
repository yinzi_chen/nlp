package dao;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.MovieReview;
import tools.SQLString;
import tools.SaveObjects;
import conf.NLPConfiguration;
import conf.NLPConstant;

/**
 * @author Yinzi Chen
 * @date Apr 13, 2014
 */
public class MovieReviewDAO {
    NLPConfiguration config;
    Connection conn;

    public MovieReviewDAO() throws Exception {
        config = new NLPConfiguration();
        config.initialize(NLPConstant.dbConfigFile);
    }

    public void connect() throws Exception {
        Class.forName(config.getProperty("db_driver"));
        conn = DriverManager.getConnection(config.getProperty("db_url"),
                config.getProperty("db_user"),
                config.getProperty("db_password"));
        System.out.println("connected to dbserver: "
                + config.getProperty("db_url"));
    }

    public void close() throws SQLException {
        conn.close();
    }

    public void addMovieReview(MovieReview review) throws SQLException {
        Statement stmt = conn.createStatement();
        String sql = "INSERT INTO movie_review VALUES (" + review.getSourceId()
                + ", " + review.getMovieId() + ", '"
                + sqlEscape(review.getDisplayTitle()) + "', '"
                + sqlEscape(review.getImgUrl()) + "', '"
                + sqlEscape(review.getOverviewUrl()) + "', '"
                + sqlEscape(review.getArticleUrl()) + "', '"
                + sqlEscape(review.getCountryStr()) + "', '"
                + sqlEscape(review.getGenreStr()) + "', '"
                + sqlEscape(review.getCategoryStr()) + "', '"
                + sqlEscape(review.getCastStr()) + "', '"
                + sqlEscape(review.getRoleStr()) + "', '"
                + sqlEscape(review.getArticleTitle()) + "', '"
                + sqlEscape(review.getPublicationDate()) + "', '"
                + sqlEscape(review.getArticle()) + "', '"
                + sqlEscape(review.getCategoryVec()) + "')";
        System.out.println(sql);
        stmt.execute(sql);
    }

    public void updateGenre(MovieReview review) throws SQLException {
        Statement stmt = conn.createStatement();
        String sql = "UPDATE movie_review SET genre_list = '"
                + sqlEscape(review.getGenreStr()) + "' WHERE source_id = "
                + review.getSourceId() + " AND movie_id = "
                + review.getMovieId();
        System.out.println(sql);
        stmt.execute(sql);
    }

    public void updateCountry(MovieReview review) throws SQLException {
        Statement stmt = conn.createStatement();
        String sql = "UPDATE movie_review SET country_list = '"
                + sqlEscape(review.getCountryStr()) + "' WHERE source_id = "
                + review.getSourceId() + " AND movie_id = "
                + review.getMovieId();
        System.out.println(sql);
        stmt.execute(sql);
    }

    public void updateCategory(MovieReview review) throws SQLException {
        Statement stmt = conn.createStatement();
        String sql = "UPDATE movie_review SET category_list = '"
                + sqlEscape(review.getCategoryStr()) + "' WHERE source_id = "
                + review.getSourceId() + " AND movie_id = "
                + review.getMovieId();
        System.out.println(sql);
        stmt.execute(sql);
    }

    public void updateImgUrl(MovieReview review) throws SQLException {
        Statement stmt = conn.createStatement();
        String sql = "UPDATE movie_review SET img_url = '"
                + sqlEscape(review.getImgUrl()) + "' WHERE source_id = "
                + review.getSourceId() + " AND movie_id = "
                + review.getMovieId();
        System.out.println(sql);
        stmt.execute(sql);
    }

    public void updateCategoryVec(MovieReview review) throws SQLException {
        Statement stmt = conn.createStatement();
        String sql = "UPDATE movie_review SET category_vector = '"
                + sqlEscape(review.getCategoryVec()) + "' WHERE source_id = "
                + review.getSourceId() + " AND movie_id = "
                + review.getMovieId();
        System.out.println(sql);
        stmt.execute(sql);
    }

    public void updateCastRoleList(MovieReview review) throws SQLException {
        Statement stmt = conn.createStatement();
        String sql = "UPDATE movie_review SET cast_list = '"
                + sqlEscape(review.getCastStr()) + "', role_list = '"
                + sqlEscape(review.getRoleStr()) + "' WHERE source_id = "
                + review.getSourceId() + " AND movie_id = "
                + review.getMovieId();
        System.out.println(sql);
        stmt.execute(sql);
    }

    public List<MovieReview> getMovieReviewByDate(String date, int offset)
            throws SQLException {
        List<MovieReview> reviewList = new ArrayList<MovieReview>();
        Statement stmt = conn.createStatement();
        String sql = "SELECT * FROM movie_review WHERE publication_date = '"
                + sqlEscape(date)
                + "' AND source_id > 0 ORDER BY publication_date limit "
                + offset + ", 20";
        // System.out.println(sql);
        ResultSet rs = stmt.executeQuery(sql);
        fillMovieReview(rs, reviewList);
        return reviewList;
    }

    public List<MovieReview> getMovieReviewByDate(String beginDate,
            String endDate, int offset) throws SQLException {
        List<MovieReview> reviewList = new ArrayList<MovieReview>();
        Statement stmt = conn.createStatement();
        String sql = "SELECT * FROM movie_review WHERE publication_date BETWEEN '"
                + sqlEscape(beginDate)
                + "' AND '"
                + sqlEscape(endDate)
                + "' AND source_id > 0 ORDER BY publication_date limit "
                + offset + ", 20";
        // System.out.println(sql);
        ResultSet rs = stmt.executeQuery(sql);
        fillMovieReview(rs, reviewList);
        return reviewList;
    }

    public List<MovieReview> getMovieReviewBasicInfoByDate(String beginDate,
            String endDate) throws SQLException {
        List<MovieReview> reviewList = null;
        try {
            reviewList = (List<MovieReview>) SaveObjects
                    .load(NLPConstant.basicInfoFile);
        } catch (IOException e) {
            reviewList = null;
        }
        if (reviewList == null) {
            reviewList = new ArrayList<MovieReview>();
        } else {
            List<MovieReview> res = new ArrayList<MovieReview>();
            for (MovieReview review : reviewList) {
                if (review.getPublicationDate().compareTo(beginDate) >= 0
                        && review.getPublicationDate().compareTo(endDate) <= 0)
                    res.add(review);
            }
            return res;
        }
        Statement stmt = conn.createStatement();
        String sql = "SELECT source_id, movie_id, display_title, img_url, overview_url,"
                + " article_url, country_list, genre_list, category_list,"
                + " cast_list, role_list,"
                + " article_title, publication_date, category_vector, movie_id as article"
                + " FROM movie_review WHERE publication_date BETWEEN '"
                + sqlEscape(beginDate)
                + "' AND '"
                + sqlEscape(endDate)
                + "' ORDER BY publication_date";
        System.out.println(sql);
        ResultSet rs = stmt.executeQuery(sql);
        fillMovieReview(rs, reviewList);
        try {
            SaveObjects.save(reviewList, NLPConstant.basicInfoFile);
        } catch (IOException e) {
            File f = new File(NLPConstant.basicInfoFile);
            f.delete();
        }
        return reviewList;
    }

    public int getMovieReviewCountByDate(String date) throws SQLException {
        int cnt;
        Statement stmt = conn.createStatement();
        String sql = "SELECT count(*) FROM movie_review WHERE publication_date = '"
                + sqlEscape(date) + "' AND source_id > 0";
        // System.out.println(sql);
        ResultSet rs = stmt.executeQuery(sql);
        rs.next();
        cnt = Integer.parseInt(rs.getString(1));
        // System.out.println(cnt);
        return cnt;
    }

    public int getMovieReviewCountByDate(String beginDate, String endDate)
            throws SQLException {
        int cnt;
        Statement stmt = conn.createStatement();
        String sql = "SELECT count(*) FROM movie_review WHERE publication_date BETWEEN '"
                + sqlEscape(beginDate)
                + "' AND '"
                + sqlEscape(endDate)
                + "' AND source_id > 0";
        // System.out.println(sql);
        ResultSet rs = stmt.executeQuery(sql);
        rs.next();
        cnt = Integer.parseInt(rs.getString(1));
        // System.out.println(cnt);
        return cnt;
    }

    private void fillMovieReview(ResultSet rs, List<MovieReview> reviewList)
            throws SQLException {
        while (rs.next()) {
            MovieReview review = new MovieReview();
            review.setSourceId(Integer.parseInt(rs.getString("source_id")));
            review.setMovieId(Integer.parseInt(rs.getString("movie_id")));
            review.setDisplayTitle(rs.getString("display_title"));
            review.setImgUrl(rs.getString("img_url"));
            review.setOverviewUrl(rs.getString("overview_url"));
            review.setArticleUrl(rs.getString("article_url"));
            review.setCountryStr(rs.getString("country_list"));
            review.setGenreStr(rs.getString("genre_list"));
            review.setCategoryStr(rs.getString("category_list"));
            review.setCastStr(rs.getString("cast_list"));
            review.setRoleStr(rs.getString("role_list"));
            review.setArticleTitle(rs.getString("article_title"));
            review.setPublicationDate(rs.getString("publication_date"));
            review.setArticle(rs.getString("article"));
            review.setCategoryVec(rs.getString("category_vector"));
            reviewList.add(review);
            // review.dump();
        }
    }

    private String sqlEscape(String str) {
        return SQLString.sqlEscape(str);
    }

}
