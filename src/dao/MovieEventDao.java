package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.MovieCountryStat;
import model.MovieReview;
import tools.SQLString;
import conf.NLPConfiguration;
import conf.NLPConstant;

/**
 * @author Yinzi Chen
 * @date Apr 29, 2014
 */
public class MovieEventDao {
    NLPConfiguration config;
    Connection conn;

    public MovieEventDao() throws Exception {
        config = new NLPConfiguration();
        config.initialize(NLPConstant.dbConfigFile);
    }

    public void connect() throws Exception {
        Class.forName(config.getProperty("db_driver"));
        conn = DriverManager.getConnection(config.getProperty("db_url"),
                config.getProperty("db_user"),
                config.getProperty("db_password"));
        System.out.println("connected to dbserver: "
                + config.getProperty("db_url"));
    }

    public void close() throws SQLException {
        conn.close();
    }

    public void updateCategory(String name, String category)
            throws SQLException {
        Statement stmt = conn.createStatement();
        String sql = "UPDATE movie_event SET category_vector = '"
                + sqlEscape(category) + "' WHERE display_title = '"
                + sqlEscape(name) + "'";
        System.out.println(sql);
        stmt.execute(sql);
    }

    public List<String[]> getNameAndCategory() throws SQLException {
        List<String[]> res = new ArrayList<String[]>();
        Statement stmt = conn.createStatement();
        String sql = "SELECT * FROM movie_event";
        // System.out.println(sql);
        ResultSet rs = stmt.executeQuery(sql);
        while (rs.next()) {
            String[] str = new String[2];
            str[0] = rs.getString("display_title");
            str[1] = rs.getString("category_vector");
            res.add(str);
        }
        return res;
    }

    private String sqlEscape(String str) {
        return SQLString.sqlEscape(str);
    }
}
