package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import tools.SQLString;

import model.MovieCountryStat;
import conf.NLPConfiguration;
import conf.NLPConstant;

/**
 * @author Yinzi Chen
 * @date Apr 26, 2014
 */
public class MovieCountryStatDAO {
    NLPConfiguration config;
    Connection conn;

    public MovieCountryStatDAO() throws Exception {
        config = new NLPConfiguration();
        config.initialize(NLPConstant.dbConfigFile);
    }

    public void connect() throws Exception {
        Class.forName(config.getProperty("db_driver"));
        conn = DriverManager.getConnection(config.getProperty("db_url"),
                config.getProperty("db_user"),
                config.getProperty("db_password"));
        System.out.println("connected to dbserver: "
                + config.getProperty("db_url"));
    }

    public void close() throws SQLException {
        conn.close();
    }

    public void addMovieCountryStat(MovieCountryStat stat) throws SQLException {
        Statement stmt = conn.createStatement();
        String sql = "INSERT INTO movie_country_stat VALUES ('"
                + sqlEscape(stat.getCountryName()) + "', " + stat.getLatitude()
                + ", " + stat.getLongitude() + ", " + stat.getActionCnt()
                + ", " + stat.getComedyCnt() + ", " + stat.getDramaCnt() + ", "
                + stat.getRomanceCnt() + ", " + stat.getCrimeCnt() + ", "
                + stat.getDocumentaryCnt() + ", " + stat.getSuspenseCnt()
                + ", " + stat.getFantasyCnt() + ", " + stat.getArtCnt() + ", "
                + stat.getAnimationCnt() + ", " + stat.getHorrorCnt() + ")";
        System.out.println(sql);
        stmt.execute(sql);
    }

    private String sqlEscape(String str) {
        return SQLString.sqlEscape(str);
    }
}
