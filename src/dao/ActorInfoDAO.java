package dao;

import java.io.IOException;
import java.util.Map;

import tools.SaveObjects;
import conf.NLPConstant;

/**
 * @author Yinzi Chen
 * @date Apr 25, 2014
 */
public class ActorInfoDAO {

    Map<String, Map<String, Integer>> map;

    public ActorInfoDAO() throws IOException {
        map = (Map<String, Map<String, Integer>>) SaveObjects
                .load(NLPConstant.castTop10File);
    }

    public ActorInfoDAO(String castFile) throws IOException {
        map = (Map<String, Map<String, Integer>>) SaveObjects.load(castFile);
    }

    public Map<String, Integer> getActorInfo(String name) throws IOException {
        Map<String, Integer> mmap = map.get(name.trim());
        System.out.println("****************");
        if (mmap == null) {
            System.out.println("No such actor: " + name);
            System.out.println("****************\n");
            return null;
        }
        System.out.println(name);
        for (String category : mmap.keySet()) {
            System.out.println(category + "(" + mmap.get(category) + ")");
        }
        System.out.println("****************\n");
        return mmap;
    }

}
