package tools;

import java.io.File;

import conf.NLPConstant;

/**
 * @author Yinzi Chen
 * @date Apr 14, 2014
 */
public class MoveFile {

    static final int dirNum = NLPConstant.articleDirNum;
    static final String dir = NLPConstant.articleDir;

    public static void main(String[] args) {
        for (int i = 0; i < dirNum; ++i) {
            File d = new File(dir + i);
            d.mkdir();
        }
        File d = new File(dir);
        for (File f : d.listFiles()) {
            if (!f.isFile())
                continue;
            File tmp = new File(dir
                    + NLPConstant.getFilePath(f.getName(), dirNum));
            // System.out.println(tmp.getPath());
            f.renameTo(tmp);
        }
    }

}
