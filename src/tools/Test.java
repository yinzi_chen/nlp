package tools;

import java.io.File;

import conf.NLPConstant;

/**
 * @author Yinzi Chen
 * @date Apr 13, 2014
 */
public class Test {

    static final int dirNum = 200;
    static final String dir = "cache/newyorktimes/detail/";

    public static void main(String[] args) {
        for (int i = 0; i < dirNum; ++i) {
            File d = new File(dir + i);
            for (File f : d.listFiles()) {
                if (f.getName().startsWith("cast")) {
                    File tmp = new File(dir
                            + NLPConstant.getFilePath("detail"
                                    + f.getName().substring(4), dirNum));
                    f.renameTo(tmp);
                }
            }
        }
    }

}
