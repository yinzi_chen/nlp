package tools;

/**
 * @author Yinzi Chen
 * @date Apr 26, 2014
 */
public class SQLString {

    public static String sqlEscape(String str) {
        if (str == null)
            return "";
        return str.replaceAll("'", "\\\\'");
    }

}
