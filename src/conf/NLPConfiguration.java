package conf;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 * @author Yinzi Chen
 * @date Apr 13, 2014
 */
public class NLPConfiguration {

    protected Properties properties;

    public NLPConfiguration() {
        properties = new Properties();
    }

    public void initialize(String propertiesFile) throws IOException {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(propertiesFile);
        } catch (FileNotFoundException e) {
            throw new IOException(e.getMessage(), e);
        }
        properties.load(fis);
    }

    public String getProperty(String name) {
        return properties.getProperty(name);
    }
}
