package conf;

import java.util.HashMap;
import java.util.HashSet;

/**
 * @author Yinzi Chen
 * @date Apr 13, 2014
 */
public class NLPConstant {
    public static final String cacheDir = "../cache/newyorktimes/moviereview/";
    public static final String articleDir = "../cache/newyorktimes/article/";
    public static final String overviewDir = "../cache/newyorktimes/overview/";
    public static final String detailDir = "../cache/newyorktimes/detail/";
    public static final String castDir = "../cache/newyorktimes/cast/";
    public static final String countryDir = "../cache/country/";
    public static final int cacheDirNum = 10;
    public static final int articleDirNum = 200;
    public static final int overviewDirNum = 200;
    public static final int detailDirNum = 200;
    public static final int castDirNum = 200;
    public static final String addressLocationBaseUrl = "http://maps.googleapis.com/maps/api/geocode/xml?sensor=false&address=";
    public static final String movieReviewBaseUrl = "http://api.nytimes.com/svc/movies/v2/reviews/search.xml?publication-date=1900-01-01;2014-12-31&order=by-publication-date&api-key=c48b42faf417417ff9062818eb531488:15:68814847&offset=";
    public static final String dbConfigFile = "conf/db.conf";
    public static final String loginCookie = "__qca=P0-197172174-1397359860673; RMID=007f01003f70534a0761005e; nyt5_disable=true; _dyid=1435233199; _dyfs=true; _cb_ls=1; kxtech=device%3DComputer%26manufacturer%3DApple%2520Inc.%26os%3DMac%2520OS%2520X%26browser%3DChrome; ebNewBandWidth_.www.nytimes.com=157%3A1397451088316; PHPSESSID=a9988202f6f7fec5b64e53f8868dac0b; NYT-Messaging=breakingnews3617-1-1397504694069; kxtag28172.day=3; kxtag27935.day=3; kxtag27728.day=3; kxtag15486.day=3; kxtag21418.day=3; kxtag22998.day=3; kxtag21233.day=3; kxtag28173.day=3; kxtag28886.day=3; upnext=0; nyt-d=101.000000000NAI00000400qU0H2nuD084mKM060MCY0fA38%2C1bSNyz0i2HLw%4025ce412%2Fe179cfcd; __utmx=69104142.YfTRFaWtTlW6xShQR00alA$0:1; __utmxx=69104142.YfTRFaWtTlW6xShQR00alA$0:1397590994:8035200; session_start_time=1397592382876; k_visit=1; __CT_Data=gpv=3&apv_202_www06=3; WRUID=0; _dyaud_page=910@-1667314095@0@1410*2014-04-16*2014-04-15%2009%3A18%3A36; _dysvar_8765260=63%253A@%253Amovies.@.64%253A@%253AMovie%2520Pages.@.65%253A@%253AMovie%2520Details; _dyuss_8765260=56; _dycst=tg.m.frv4.ms.ltos.ah.c.fst.; _dy_geo=US.NA; _dyaud_nchc=910*1410@16*1410@41*910@nu; _dyaud_sess=**; _dycnoabc=1397595607267; nytnow3p=2; nyt-recommend=1; tagx-l=sessionIndex%3D54%26sessionStart%3D1397596388650%26isNewSession%3D1%26lastRequest%3D1397596388650%26prevRequest%3D1397595553496%26firstReferrer%3Dhttp%253A%252F%252Fdeveloper.nytimes.com%252Fdocs%252Fmovie_reviews_api%252F%26firstLanding%3Dhttp%253A%252F%252Fwww.nytimes.com%252Fmovies%252Fcritics%252Fcritics-picks%26firstSeen%3D1397360501845%26browserSession%3D1; tagx-s=referrer%3Dhttp%253A%252F%252Fdeveloper.nytimes.com%252Fdocs%252Fmovie_reviews_api%252F%26landing%3Dhttp%253A%252F%252Fwww.nytimes.com%252Fmovies%252Fcritics%252Fcritics-picks%26start%3D1397360501845%26isNew%3D0%26pageIndex%3D63; tagx-p=prevPage%3Dhttp%253A%252F%252Fwww.nytimes.com%252Fmovies%252Fmovie%252F156915%252FShooting-Porn%252Foverview%26currPage%3Dhttp%253A%252F%252Fwww.nytimes.com%252F2008%252F02%252F29%252Fmovies%252F29chen.html%253Fgwh%253D6DC019322037C1055882C65282E6455A%2526gwt%253Dpay; WT_FPC=id=e2ba971b-a3f4-444f-8829-e7c509de4e6d:lv=1397596391426:ss=1397595348711; rsi_segs=D08734_72771|D08734_70012|D08734_70008|D08734_70033|D08734_70086|D08734_71095|D08734_70619|D08734_72791|D08734_70035|D08734_72774|D08734_70094|D08734_70027|D08734_70045|D08734_70044|D08734_70026|D08734_72016|D08734_72103|D08734_70101|D08734_72014|D08734_72018|D08734_70055|D08734_72414|D08734_70017|D08734_70021|D08734_70030|D08734_70031|D08734_70041|D08734_70047|D08734_70050|D08734_70057|D08734_70059|D08734_70062|D08734_70063|D08734_70075|D08734_70076; _chartbeat2=BbMSNOC6qLqSUMaDi.1397360506458.1397596392067.1111; _chartbeat_uuniq=3; kxprvx_segs=ocyn71yun%2Cm5qu6ck2u%2Cmu2yrhi20%2Cm5m1krl33; kxuser=IsTmZHyt; kxkuid=IsTmZHyt; kxsegs=ocyn71yun%2Cm5qu6ck2u%2Cmu2yrhi20%2Cm5m1krl33; krux_segs=ocyn71yun%7Cm5qu6ck2u%7Cmu2yrhi20%7Cm5m1krl33; nyt-a=bb60b8f7c08bf410a1ffd72cf4f41aac; _gab=GA1.2.1313659822.1397590986; abTest_experiments=%7B%22YfTRFaWtTlW6xShQR00alA%22%3A%7B%22ts%22%3A1397596689736%2C%22ttl%22%3A1800000%2C%22vendor%22%3A%22ga%22%2C%22env%22%3A%22live%22%2C%22dimension3%22%3A%22gw%22%2C%22expId%22%3A%22YfTRFaWtTlW6xShQR00alA%22%2C%22expVar%22%3A1%7D%7D; abTest_orderForm_evt=%7B%22expId%22%3A%22YfTRFaWtTlW6xShQR00alA%22%2C%22time%22%3A1397596689738%7D; abTestLogLevel=0; NYT-S=185g0pf11pUTQ1SoeQjuXdR0h7uZpmkvMFXmX64Uxa.xiYB4WipyCzCLu766Q1EJr3gGzxvbxRW5uPIW.hBW8IsZkZAkWYZYr1; _chartbeat5=362,624,%2Fmovies%2Fmovie%2F156915%2FShooting-Porn%2Foverview,http%3A%2F%2Fwww.nytimes.com%2Fmovie%2Freview%3Fres%3D9F0DE3DB1039F932A25754C0A961958260; adxcl=l*31073=5695d9cf:1; adxcs=s*3b17f=0:1|s*3b187=0:1|s*3b397=0:1|si=0:1; _dyus_8765260=825%7C542%7C1%7C42%7C0%7C0.0.1397360502989.1397595556147.235053.0%7C104%7C16%7C3%7C114%7C24%7C17%7C12%7C3%7C0%7C0%7C0%7C53%7C3%7C0%7C0%7C0%7C0%7C56%7C0%7C0%7C0%7C0%7C0; _cb_cp=BkQObSDpY9ofBwgZDyDM6NaGiH7sU; _chartbeat4=t=BkQObSDpY9ofBwgZDyDM6NaGiH7sU&E=23&x=440&c=28.61&y=1182&w=323; nyt-m=BD0658B1FDBBDD9EDB03F87C28461399&e=i.1398916800&t=i.10&v=i.14&l=l.25.3386842213.3457048873.2760159741.3252337533.2117548572.2832902099.174088242.2732010557.1418720596.2357234122.835247851.3285134383.290433060.1410407104.-1.-1.-1.-1.-1.-1.-1.-1.-1.-1.-1&n=i.2&g=i.10&rc=i.0&er=i.1396746722&vr=l.4.14.0.0.0&pr=l.4.60.0.4.0&vp=i.0&gf=l.10.3386842213.3457048873.2760159741.3252337533.2117548572.2832902099.174088242.2732010557.1418720596.2357234122&ft=i.0&fv=i.0&gl=l.5.835247851.3285134383.290433060.1410407104.-1&cav=i.14&imu=i.1&igu=i.1&prt=i.5&ica=i.1&iue=i.0&ier=i.0&iub=i.0&ifv=i.0&igd=i.1&iga=i.1&imv=i.0&igf=i.0&iru=i.1&ird=i.0&ira=i.1&iir=i.1&kid=i.1&rl=l.1.-1";
    public static final String mappingFile = "conf/genre_category_mapping_new.txt";
    public static final HashSet<String> categories;
    public static final HashMap<String, Integer> category2Idx;
    public static final String basicInfoFile = "conf/basic_info.obj";
    public static final String castTop4File = "conf/cast_top_4.obj";
    public static final String castTop6File = "conf/cast_top_6.obj";
    public static final String castTop10File = "conf/cast_top_10.obj";
    public static final String castAllFile = "conf/cast_all.obj";

    static {
        categories = new HashSet<String>();
        category2Idx = new HashMap<String, Integer>();
        // categories.add("action");
        // categories.add("comedy");
        // categories.add("art");
        // categories.add("fantasy");
        // categories.add("drama");
        // categories.add("horror");
        categories.add("action & adventure");
        categories.add("comedy");
        categories.add("drama");
        categories.add("romance");
        categories.add("crime");
        categories.add("documentary & biography");
        categories.add("mystery & suspense");
        categories.add("science fiction & fantasy");
        categories.add("art");
        categories.add("animation & cartoons");
        categories.add("horror");
        category2Idx.put("action & adventure", 0);
        category2Idx.put("comedy", 1);
        category2Idx.put("drama", 2);
        category2Idx.put("romance", 3);
        category2Idx.put("crime", 4);
        category2Idx.put("documentary & biography", 5);
        category2Idx.put("mystery & suspense", 6);
        category2Idx.put("science fiction & fantasy", 7);
        category2Idx.put("art", 8);
        category2Idx.put("animation & cartoons", 9);
        category2Idx.put("horror", 10);
    }

    public static String getFilePath(String fileName, int dirNum) {
        return Math.abs(fileName.hashCode()) % dirNum + "/" + fileName;
    }

}
