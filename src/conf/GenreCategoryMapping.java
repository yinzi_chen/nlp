package conf;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

/**
 * @author Yinzi Chen
 * @date Apr 22, 2014
 */
public class GenreCategoryMapping {

    private HashMap<String, String> mapping;

    public GenreCategoryMapping() {
        mapping = new HashMap<String, String>();
    }

    public void initialize(String propertiesFile) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(
                new FileInputStream(propertiesFile)));
        String line;
        while ((line = br.readLine()) != null && !line.trim().equals("")) {
            String[] kv = line.split("=");
            mapping.put(kv[0].trim().toLowerCase(), kv[1].trim().toLowerCase());
        }
        br.close();
    }

    public String getProperty(String name) {
        return mapping.get(name);
    }
}
