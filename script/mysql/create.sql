USE nlp;
CREATE TABLE `movie_review` (
  `source_id` int(32) NOT NULL DEFAULT '0',
  `movie_id` int(32) NOT NULL DEFAULT '0',
  `display_title` varchar(128) DEFAULT NULL,
  `img_url` varchar(512) DEFAULT NULL,
  `overview_url` varchar(512) DEFAULT NULL,
  `article_url` varchar(512) DEFAULT NULL,
  `country_list` varchar(64) DEFAULT NULL,
  `genre_list` varchar(512) DEFAULT NULL,
  `category_list` varchar(128) DEFAULT NULL,
  `cast_list` text,
  `role_list` text,
  `article_title` varchar(128) DEFAULT NULL,
  `publication_date` date DEFAULT NULL,
  `article` text,
  `category_vector` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`source_id`,`movie_id`),
  KEY `movie_id` (`movie_id`),
  KEY `publication_date` (`publication_date`),
  FULLTEXT KEY `display_title` (`display_title`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `movie_country_stat` (
  `country_name` varchar(64) NOT NULL DEFAULT '',
  `latitude` float(10,6) DEFAULT '0.000000',
  `longitude` float(10,6) DEFAULT '0.000000',
  `action_cnt` int(32) DEFAULT '0',
  `comedy_cnt` int(32) DEFAULT '0',
  `drama_cnt` int(32) DEFAULT '0',
  `romance_cnt` int(32) DEFAULT '0',
  `crime_cnt` int(32) DEFAULT '0',
  `documentary_cnt` int(32) DEFAULT '0',
  `suspense_cnt` int(32) DEFAULT '0',
  `fantasy_cnt` int(32) DEFAULT '0',
  `art_cnt` int(32) DEFAULT '0',
  `animation_cnt` int(32) DEFAULT '0',
  `horror_cnt` int(32) DEFAULT '0',
  PRIMARY KEY (`country_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `movie_event` (
  `movie_id` int(32) NOT NULL AUTO_INCREMENT,
  `display_title` varchar(128) DEFAULT NULL,
  `article_title` varchar(128) DEFAULT NULL,
  `article` text,
  `category_vector` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`movie_id`),
  KEY `display_title` (`display_title`(16))
) ENGINE=MyISAM AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;

