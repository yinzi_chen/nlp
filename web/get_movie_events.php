<?php 

include_once 'common.php';
include_once 'google_movie_event_tag.php';

function getMovieEvents($address, $force = false) {
	$baseUrl = "http://www.google.com/movies?sort=1&near=";
	$url = $baseUrl.$address."&start=0";
	$movie_events = array();
	do {
		// 		echo "***********************<br>\n";
		// 		echo $url."<br><br>\n";
		$file = '../../cache/movie_events_cache/'.get_clean_url($url).'-'.date('Y-m-d').'.html';
		getContent($url, $file, $force);
		phpQuery::newDocumentFile($file);
		$url = get_page_url();
		$movies= get_movies();
		foreach ($movies as $m) {
			$movie_title = get_movie_title($m);
			$movie_url = get_movie_url($m);
			$movie_img = get_movie_img($m);
			if (strpos($movie_img, 'miss') <> null) {
				$movie_img = "http://www.google.com".$movie_img;
			}
			$movie_desc = get_movie_desc($m);
			$cinemas = get_cinemas($m);
			// 			echo $movie_title."****".$movie_url."****".$movie_img."<br>\n";
			// 			echo $movie_desc."<br>\n";
			$movie_data = array();
			global $lat, $lng;
			foreach ($cinemas as $c) {
				$cinema_name = get_cinema_name($c);
				$cinema_url = get_cinema_url($c);
				$cinema_location = get_cinema_location($c);
				getLatLng($cinema_location);
				$cinema_lat = $lat;
				$cinema_lng = $lng;
				$show_time = get_show_time($c);
				// 				echo $cinema_name."****".$cinema_url."****".$cinema_location."<br>\n";
				array_push($movie_data, ['cinema_name'=>$cinema_name, 'cinema_url'=>$cinema_url, 'cinema_location'=>$cinema_location, 'cinema_lat'=>$cinema_lat, 'cinema_lng'=>$cinema_lng, 'show_time'=>$show_time]);
			}
			array_push($movie_events, ['movie_title'=>$movie_title, 'movie_url'=>$movie_url, 'movie_img'=>$movie_img, 'movie_desc'=>$movie_desc, 'movie_data'=>$movie_data]);
			// 			echo "<br>\n";
		}
		phpQuery::$documents = null;
	} while ($url <> null);
	return $movie_events;
}

$movie_events = getMovieEvents(75252);
// print_r($movie_events);

?>
