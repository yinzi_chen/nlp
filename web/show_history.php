<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no">
<meta charset="utf-8">
<title>Info window with <code>maxWidth</code>
</title>
<style>
html,body,#map-canvas {
	height: 100%;
	margin: 0px;
	padding: 0px
}
</style>
<script
	src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script>
		var i = null;
		var map;
		var markers = [];
		<?php
			set_time_limit(0);
			ob_implicit_flush(1);

			include_once 'common.php';
			
			function getCountryData() {
				$errors = 0;
				$DBConnect = @mysql_connect("cloudhadoop", "nlp", "nlp_proj");
				if ($DBConnect === FALSE) {
					echo "<p>Unable to connect to the database server.</p>"
							."<p>Error code ".mysql_errno().": ".mysql_error()."</p>";
					++$errors;
				} else {
					$DBName = "nlp";
					$result = @mysql_select_db($DBName, $DBConnect);
					if ($result === FALSE) {
						echo "<p>Unable to select the database. ".
								"Error code ".mysql_errno($DBConnect).": ".
								mysql_error($DBConnect)."</p>\n";
						++$errors;
					}
				}
				
				$TableName = "movie_country_stat";
				if ($errors == 0) {
					$SQLString = "SELECT * FROM $TableName";
					$QueryResult = @mysql_query($SQLString, $DBConnect);
					if ($QueryResult === FALSE) {
						echo "<p>Unable to execute the query. ".
								"Error code ".mysql_errno($DBConnect).": ".
								mysql_error($DBConnect)."</p>\n";
						++$errors;
					}
				}
				
				if ($errors == 0) {
					$countryData = array();
					while (($Row = mysql_fetch_assoc($QueryResult)) !== FALSE) {
						$sum = $Row['action_cnt'] + $Row['comedy_cnt'] + $Row['drama_cnt']
							+ $Row['romance_cnt'] + $Row['crime_cnt'] + $Row['documentary_cnt']
							+ $Row['suspense_cnt'] + $Row['fantasy_cnt'] + $Row['art_cnt']
						    + $Row['animation_cnt'] + $Row['horror_cnt'];
						if ($sum < 10) continue;
						array_push($countryData, [$Row['country_name'],
								$Row['latitude'], $Row['longitude'],
								[
								['action', $Row['action_cnt']],
								['comedy', $Row['comedy_cnt']],
								['drama', (int)($Row['drama_cnt'] / 2)],
								['romance', $Row['romance_cnt']],
								['crime', $Row['crime_cnt']],
								['documentary', $Row['documentary_cnt']],
								['suspense', $Row['suspense_cnt']],
								['fantasy', $Row['fantasy_cnt']],
								['art', $Row['art_cnt']],
								['animation', $Row['animation_cnt']],
								['horror', $Row['horror_cnt']],
								]
								]);
					}
					mysql_free_result($QueryResult);
				}
				return $countryData;
			}
			
			function getContinentData() {
				return [['Asia', 34.0478630, 100.6196553, [['drama', 3], ['war', 2], ['comedy', 5], ['art', 3]]], ['Europe', 54.5259614, 15.2551187, [['drama', 4], ['war', 2], ['comedy', 5], ['art', 2]]], ['North America', 54.5259614, -105.2551187, [['drama', 23], ['war', 12], ['comedy', 15], ['art', 3]]]];
			}
			
			function getJsStr($countries) {
				$str = "";
				$str .= "[";
				foreach ($countries as $country) {
					$str .= "[";
					$str .= "'".$country[0]."',";
					$str .= "'".$country[1]."',";
					$str .= "'".$country[2]."',";
					$str .= "[";
					foreach ($country[3] as $data) {
						$str .= "[";
						$str .= "'".$data[0]."',".$data[1]."],";
					}
					$str .= "]";
					$str .= "],";
				}
				$str .= "]";
				return $str;
			}
			$continentStr = getJsStr(getContinentData());
			$countryStr = getJsStr(getCountryData());
			echo "
				var continentdata = $continentStr;
				var countrydata = $countryStr;
			";
		?>

		function addMarkers(zoom) {
			var data;
			if (zoom > 0) {
				data = countrydata;
			} else {
				data = continentdata;
			}
			for (var i = 0; i < data.length; i++)
			{
				addMarker(data[i]);
			}
		}

		function addMarker(data) {
			var marker = new google.maps.Marker({
			    position: new google.maps.LatLng(data[1], data[2]),
			    map: map
			});
			markers.push(marker);
		    google.maps.event.addListener(marker, 'click', function() {
		    	drawChart(this, data);
		    });
		}
		function clearMarkers() {
			for (var i = 0; i < markers.length; i++) {
		    	markers[i].setMap(null);
			}
			markers = [];
		}
					
	    function drawChart(marker, data) {

    	    // Create the data table.
    	    var table = new google.visualization.DataTable();
    	    table.addColumn('string', 'Topping');
    	    table.addColumn('number', 'Slices');
    	    table.addRows(data[3]);
    	    
    	    // Set chart options
    	    var options = {'title':data[0]+' @ '+
    	                   marker.getPosition().toString(),
    	                   'width':300,
    	                   'height':200};

    	    var node = document.createElement('div'),
    	    infoWindow = new google.maps.InfoWindow(),
    	    chart = new google.visualization.PieChart(node);
    	    chart.draw(table, options);
    	    if (i != null) i.close();
    	    infoWindow.setContent(node);
    	    infoWindow.open(marker.getMap(),marker);
    	    i = infoWindow;
    	}

		var zoom = 2;
		
		function initialize() {
		    var mapOptions = {
		      center: new google.maps.LatLng(32.3154070,18.3568920),
		      zoom: zoom,
		      mapTypeId: google.maps.MapTypeId.ROADMAP
		    };
		    
		    map = new google.maps.Map(document.getElementById("map-canvas"),
		        mapOptions);

			addMarkers(zoom);
			
		    google.maps.event.addListener(map, 'click', function() {
		        if (i != null) i.close();
		    });
		    google.maps.event.addListener(map, 'zoom_changed', function() {
		    	if (i != null) i.close();
				clearMarkers();
		        var zoom = map.getZoom();
		        addMarkers(zoom);
		      });
		}
		</script>
<script>
		    google.load('visualization', '1.0', {'packages':['corechart']});
		    google.maps.event.addDomListener(window, 'load', initialize);
		</script>
</head>
<body>
	<div id="map-canvas"></div>
</body>
</html>

