<?php

include_once 'phpQuery/phpQuery/phpQuery.php';

function get_page_url() {
	$pages = pq('#navbar')->find('[class="b"]');
	foreach ($pages as $page)
		if (strpos(pq($page)->text(), 'Next') !== FALSE)
			return "http://www.google.com".pq($page)->find('a')->attr('href');
	return null;
}

function get_movies() {
	return pq("#movie_results")->find('[class="movie"]');
}

function get_movie_title($m) {
	return pq($m)->find('[itemprop="name"]')->find('a')->text();
}

function get_movie_url($m) {
	return pq($m)->find('[itemprop="name"]')->find('a')->attr('href');
}

function get_movie_img($m) {
	return pq($m)->find('[class="img"]')->find('img')->attr('src');
}

function get_movie_desc($m) {
	return pq($m)->find('[class="desc"]')->find('[itemprop="description"]')->text();
}

function get_cinemas($m) {
	return pq($m)->find('[class="showtimes"]')->find('[class="theater"]');
}

function get_cinema_name($c) {
	return pq($c)->find('[class="name"]')->find('a')->text();
}

function get_cinema_url($c) {
	return pq($c)->find('[class="name"]')->find('a')->attr('href');
}

function get_cinema_location($c) {
	$addr = pq($c)->find('[class="address"]')->text();
	// 	$suffix = pq($c)->find('[class="address"]')->find('a')->text();
	// 	return substr($addr, 0, strpos($addr, $suffix));
	return $addr;
}

// function get_movie_info($m) {
// 	return pq($m)->find('[class="info"]')->contents();
// }

function get_show_time($c) {
	$str = pq($c)->find('[class="times"]')->contents();
	$str = str_replace('&lrm;', '', $str);
	$str = str_replace('&nbsp;', '', $str);
	$str = str_replace('&nbsp', '', $str);
	$str = str_replace('nbsp', '', $str);
	$str = str_replace('&', '', $str);
	$str = str_replace('amp', '', $str);
	$str = str_replace(';', '', $str);
	$str = str_replace("/url", "http://www.google.com/url", $str);
	return $str;
}

?>