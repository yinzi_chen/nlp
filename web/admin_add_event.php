<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>
Admin Add Event
</title>
</head>

<body>

<h1>Admin Add Event</h1>
<?php 
$errors = 0;
$movie_title = "";
$category_list = "";
if (empty($_POST['movie_title']) || empty($_POST['category_list'])) {
	++$errors;
	echo "<p>You need to enter all the require fields.</p>\n";
} else {
	$movie_title = stripslashes($_POST['movie_title']);
	$category_list = stripslashes($_POST['category_list']);
}
if ($errors == 0) {
	$DBConnect = @mysql_connect("cloudhadoop", "nlp", "nlp_proj");
	if ($DBConnect === FALSE) {
		echo "<p>Unable to connect to the database server. ".
				"Error code: ".mysql_errno().": ".
				mysql_error()."</p>\n";
		++$errors;
	} else {
		$DBName = "nlp";
		$result = @mysql_select_db($DBName, $DBConnect);
		if ($result === FALSE) {
			echo "<p>Unable to select the database. ".
					"Error code ".mysql_errno($DBConnect).
					": ".mysql_error($DBConnect)."</p>\n";
			++$errors;
		}
	}
}
if ($errors > 0) {
	echo "<p>Please use your browser's BACK button to return".
			" to the form and fix the errors indicated.</p>\n";
}
if ($errors == 0) {
	$category_vector = array();
	for ($i = 0; $i < 11; ++$i) {
		array_push($category_vector, 0);
	}
	$vec = explode(',', $category_list);
	for ($i = 0; $i < sizeof($vec); ++$i) {
		if (strcasecmp($vec[$i], "action & adventure") == 0) {
			$category_vector[0] = 1;
		} else if (strcasecmp($vec[$i], "comedy") == 0) {
			$category_vector[1] = 1;
		} else if (strcasecmp($vec[$i], "drama") == 0) {
			$category_vector[2] = 1;
		} else if (strcasecmp($vec[$i], "romance") == 0) {
			$category_vector[3] = 1;
		} else if (strcasecmp($vec[$i], "crime") == 0) {
			$category_vector[4] = 1;
		} else if (strcasecmp($vec[$i], "documentary & biography") == 0) {
			$category_vector[5] = 1;
		} else if (strcasecmp($vec[$i], "mystery & suspense") == 0) {
			$category_vector[6] = 1;
		} else if (strcasecmp($vec[$i], "science fiction & fantasy") == 0) {
			$category_vector[7] = 1;
		} else if (strcasecmp($vec[$i], "art") == 0) {
			$category_vector[8] = 1;
		} else if (strcasecmp($vec[$i], "animation & cartoons") == 0) {
			$category_vector[9] = 1;
		} else if (strcasecmp($vec[$i], "horror") == 0) {
			$category_vector[10] = 1;
		} 
	}
	
	$str = $category_vector[0];
	for ($i = 1; $i < 11; ++$i) {
		$str .= ",".$category_vector[$i];
	}
	
	$TableName = "movie_event";
	$movie_title = mysql_real_escape_string($movie_title);
	$str = mysql_real_escape_string($str);
	$SQLString = "INSERT INTO $TableName ".
			" (display_title, category_vector) ".
			" VALUES('$movie_title', '$str')";
	$QueryResult = @mysql_query($SQLString, $DBConnect);
	if ($QueryResult === FALSE) {
		echo "<p>Unable to add the movie event ".
				" information. Error code ".
				mysql_errno($DBConnect).": ".
				mysql_error($DBConnect)."</p>\n";
		++$errors;
	}
	mysql_close($DBConnect);
}
if ($errors == 0) {
	echo "<p>The movie $movie_title has been added successfully</p>\n";
	echo "<a href=\"admin.php\">Add a new one</a>\n";
}
?>

</body>

</html>
