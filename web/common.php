<?php
include_once 'curl.php';
function get_similarity($s1, $s2) {
	if ($s1 === null || $s2 === null || strlen(trim($s1)) == 0 || strlen(trim($s2)) == 0) {
		return 0.0;
	}
	$v1 = explode(',', $s1);
	$v2 = explode(',', $s2);
	if (sizeof($v1) != sizeof($v2)) {
		return 0.0;
	}
	$score = 0;
	$p = 0;
	$q = 0;
	for ($i = 0; $i < sizeof($v1); ++$i) {
		$score += $v1[$i] * $v2[$i];
		$p += $v1[$i] * $v1[$i];
		$q += $v2[$i] * $v2[$i];
	}
	if ($p == 0 || $q == 0) return 0.0;
	return $score / sqrt($p) / sqrt($q);
}

function get_clean_url($url) {
	return preg_replace('/[^\p{L}\p{N}]/u','_',$url);
}

function getLatLng($addr) {
	$url = 'http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($addr).'&sensor=false';
	$file = '../../cache/cinema_location_cache/'.get_clean_url($addr).'.json';
	if (!file_exists($file) || filesize($file) < 512)
		getContent($url, $file);
	$jsonStr = file_get_contents($file);
	$json = json_decode($jsonStr, true);
	//print_r($json);
	global $lat, $lng;
	$lat = isset($json["results"][0]["geometry"]["location"]["lat"])?$json["results"][0]["geometry"]["location"]["lat"]:100000;
	$lng = isset($json["results"][0]["geometry"]["location"]["lng"])?$json["results"][0]["geometry"]["location"]["lng"]:100000;
}

function getContent($url, $file, $force = false) {
	//echo "...........\n";
	// 	$url_clean = preg_replace('/[^\p{L}\p{N}]/u','_',$url);
	if (!$force && file_exists($file) && file_validation($file)) return false;
	$ch = curl_init();
	//curl_setopt($ch, CURLOPT_PROXY ,'198.102.28.100:8089');
	curl_setopt($ch,CURLOPT_FOLLOWLOCATION,TRUE);
	curl_setopt($ch, CURLOPT_URL, $url);
	//rl_setopt($ch, CURLOPT_HEADER, true);
	curl_setopt($ch, CURLOPT_TIMEOUT, 30);
	// 	curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	$str = curl_exec($ch);
	file_put_contents($file, $str);
	// 	file_put_contents(cachedir.'/'.$url_clean.'.html',$str,FILE_APPEND);
	// 	rename(cachedir.'/'.$url_clean.'.txt',cachedir.'/'.$url_clean.'.html');
	curl_close($ch);
	return true;
}

function file_validation($file) {
	if(filesize($file) < 5120) {
		return false;
	}
	return true;
}
?>