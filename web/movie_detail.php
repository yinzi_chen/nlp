<?php 
include_once 'common.php';
include_once 'get_movie_events.php';
include_once 'get_movie_detail.php';

$force_update = false;
if (isset($_GET['search_title']))
	$search_title = $_GET['search_title'];
else
	$search_title = NULL;
if ($search_title === NULL || strlen(trim($search_title)) == 0) {
	$search_title = "Tyler Perry's Single Moms Club";
}
if (isset($_GET['zip_code']))
	$zip_code = $_GET['zip_code'];
else
	$zip_code = NULL;
if ($zip_code === NULL || strlen(trim($zip_code)) == 0) {
	$zip_code = 75252;
}

$movie_events = getMovieEvents($zip_code, $force_update);
$movie_detail = getMovieDetail($search_title);
if ($movie_detail === null) {
	echo "No such movie found.<br>\n";
	return;
}
$movie_event_scores = array();
$movie_events_top4 = array();
foreach ($movie_events as $movie_event) {
	$movie_event_score = getMovieEventDetail($movie_event["movie_title"]);
	if ($movie_event_score === null) {
		$score = 0.0;
	} else {
// 		echo $movie_event_score['display_title']." ".$movie_detail['category_vector']." ".$movie_event_score['category_vector']."<br>\n";
		$score = get_similarity($movie_event_score['category_vector'], $movie_detail['category_vector']);
	}
	array_push($movie_event_scores, $score);
}
$select_idx = array();
for ($i = 0; $i < 8; ++$i) {
	$max = -1;
	for ($j = 0; $j < sizeof($movie_event_scores); ++$j) {
		if (in_array($j, $select_idx)) continue;
		if ($max == -1 || $movie_event_scores[$j] > $movie_event_scores[$max]) {
			$max = $j;
		}
	}
	array_push($select_idx, $max);
	array_push($movie_events_top4, $movie_events[$max]);
}
for ($i = 0; $i < 4; ++$i) {
	$k = rand(2, 7-$i);
	array_splice($movie_events_top4, $k, 1);
}
$display_title = $movie_detail['display_title'];
$img_url = $movie_detail['img_url'];
$overview_url = $movie_detail['overview_url'];
$cast_url = substr($movie_detail['overview_url'], 0, strpos($movie_detail['overview_url'], 'overview'))."cast";
$article_url = $movie_detail['article_url'];
$article = str_replace('?', '', $movie_detail['article']);
$country_list = $movie_detail['country_list'];
$genre_list = $movie_detail['category_list'];
$cast_list = $movie_detail['cast_list'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link rel="stylesheet" type="text/css"
			href="http://graphics8.nytimes.com/css/0.1/screen/build/movies/overview/styles.css">
		<title>Movie Detail</title>
		<style>
			html, body, #shell, #page, #main, #l1, #l2, #map-canvas {
				height: 95%;
				margin: 0px;
				padding: 0px
			}
			html, body, #shell {TEXT-ALIGN: left; margin-left: auto; margin-right: auto}
/* 			#center { MARGIN-RIGHT: auto; MARGIN-LEFT: auto; }  */
			table { 
				table-layout: fixed;
				word-wrap:break-word;
			}
			div {
				word-wrap:break-word;
			}
		</style>
		<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
		<script src="http://www.google.com/jsapi"></script>
		<script>
			var info = null;
			var lat = <?php getLatLng($zip_code); echo $lat?>;
			var lng = <?php getLatLng($zip_code); echo $lng?>;
			var markers = [];
			var map;
			var movie_title;
			<?php
				$js_array = json_encode($movie_events_top4);
				echo "var movie_events_top4 = ". $js_array . ";\n";
			?>

			function addMarkers(data) {
				for (var i = 0; i < data.length; i++)
				{
					addMarker(data[i]);
				}
			}
			function addMarker(data) {
				var marker = new google.maps.Marker({
				    position: new google.maps.LatLng(data['cinema_lat'], data['cinema_lng']),
				    map: map
				});
				markers.push(marker);
			    google.maps.event.addListener(marker, 'click', function() {
			        if (info != null) info.close();
			    	var contentString = '<a href="http://www.google.com'+data['cinema_url']+'"><strong>'+data['cinema_name']+'</strong></a><br>'
			    		+data['cinema_location']+'<br><strong>'+movie_title+'</strong>\'s show time:<br>'+data['show_time'];
			    	var infowindow = new google.maps.InfoWindow({
			    	      content: contentString
			    	  });
			    	info = infowindow;
			    	infowindow.open(map, marker);
			    });
			}
			function clearMarkers() {
				if (info != null) info.close();
				for (var i = 0; i < markers.length; i++) {
			    	markers[i].setMap(null);
				}
				markers = [];
			}
		 	function movie_click(idx) {
		 		map.setZoom(10);
		 	    map.setCenter(new google.maps.LatLng(lat, lng));
		 	    clearMarkers();
		 	    movie_title = movie_events_top4[idx]["movie_title"];
		 	    addMarkers(movie_events_top4[idx]["movie_data"]);
		 	}
			function initialize() {
		    	var mapOptions = {
				      center: new google.maps.LatLng(lat, lng),
				      zoom: 10,
				      mapTypeId: google.maps.MapTypeId.ROADMAP
			    };
			    map = new google.maps.Map(document.getElementById("map-canvas"),
			        mapOptions);
			    google.maps.event.addListener(map, 'click', function() {
			        if (info != null) info.close();
			    });
			}
		 	google.maps.event.addDomListener(window, 'load', initialize);
		</script>
	</head>

	<body>
		<div id="shell" style="vertical-align:middle">
			<div class="singleRule"></div>
		    <div class="column firstColumn">
		        <div class="columnGroup">
			        <div class="inlineSearchControl">
		                <form name="zip" method="get" action="movie_detail.php">
		                	<input type="hidden" name="search_title" class="text" value="<?php echo $search_title;?>" placeholder="enter the movie name">
		                    <input type="text" name="zip_code" class="text" value="<?php echo $zip_code;?>" placeholder="enter your zip code">
		                    <input type="submit" value="Update Zip">
		                </form>
		            </div>
		        </div>
	        </div>
	        <div class="column two lastColumn">
		        <div class="columnGroup">
			        <div class="inlineSearchControl">
		                <form name="movie" method="get" action="movie_detail.php">
			                <input type="hidden" name="zip_code" class="text" value="<?php echo $zip_code;?>" placeholder="enter your zip code">
		                    <input type="text" name="search_title" class="text" value="<?php echo $search_title;?>" placeholder="enter the movie name">
		                    <input type="submit" value="Search Movie">
		                </form>
		            </div>
		        </div>
	        </div>
			<div class="singleRule"></div>
			<div id="page" class="tabContent active">
				<div id="main">
					<div id="l1" class="wideA">
						<div class="aColumn">
							<div class="columnGroup headlineModule">
								<h2>
									<span itemprop="name"><?php echo $display_title?> </span>
								</h2>
							</div>
							<div class="doubleRuleDivider"></div>
							<div id="reviewDetails" class="columnGroup movieDetails wrap">
								<div class="runaroundRight">
									<img src="<?php echo $img_url;?>" alt=""
										onerror="this.style.display='none'"> </img>
								</div>
								<h3 class="sectionHeader">Review Summary</h3>
								<p class="summary reviewSummary">
									<?php echo substr($article, 0, 200);?>...
								</p>
								<!--<a href="<?php echo $overview_url;?>"> </a>-->
								<div class="columnGroup first">
									<p class="refer">
										<a href="<?php echo $article_url;?>">Full New York Times Review</a>
									</p>
								</div>
								<div class="singleRule">
									<div id="movieDetails" class="movieDetails">
										<h3 class="sectionHeader">Movie Details</h3>
										<ul class="meta flush">
											<li><strong>Title: </strong> <?php echo $display_title;?></li>
											<li><strong>Country: </strong> <?php echo $country_list;?></li>
											<li><strong>Genre: </strong> <?php echo $genre_list;?></li>
											<li><strong>Cast: </strong>
												<?php echo substr($cast_list, 0, 40);?>
												<a href="<?php echo $cast_url; ?>">...</a>
											</li>
										</ul>
									</div>
								</div>
								<div class="singleRule">
									<h3 class="sectionHeader">Similar Movies (Click image to show on map)</h3>
									<table border='0' width='100%'>
										<tr>
											<td align="center"><img width=75 height=75 onclick="movie_click(0)" src=<?php echo $movie_events_top4[0]['movie_img']?>></td>
											<td align="center"><img width=75 height=75 onclick="movie_click(1)" src=<?php echo $movie_events_top4[1]['movie_img']?>></td>
											<td align="center"><img width=75 height=75 onclick="movie_click(2)" src=<?php echo $movie_events_top4[2]['movie_img']?>></td>
											<td align="center"><img width=75 height=75 onclick="movie_click(3)" src=<?php echo $movie_events_top4[3]['movie_img']?>></td>
										</tr>
										<tr>
											<td align="center" valign="top"><?php echo substr($movie_events_top4[0]['movie_title'],0,min(100,strlen($movie_events_top4[0]['movie_title'])))?></td>
											<td align="center" valign="top"><?php echo substr($movie_events_top4[1]['movie_title'],0,min(100,strlen($movie_events_top4[1]['movie_title'])))?></td>
											<td align="center" valign="top"><?php echo substr($movie_events_top4[2]['movie_title'],0,min(100,strlen($movie_events_top4[2]['movie_title'])))?></td>
											<td align="center" valign="top"><?php echo substr($movie_events_top4[3]['movie_title'],0,min(100,strlen($movie_events_top4[3]['movie_title'])))?></td>
										</tr>
									</table>
								</div>
							</div>
						</div>
						<div id="l2" class="bcColumn">
							<div class="columnGroup headlineModule">
								<h2>
									<span itemprop="name"><?php echo "<br>"?> </span>
								</h2>
							</div>
							<div class="doubleRuleDivider"></div>
							<div id="map-canvas"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
