<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>
Add Movie Event
</title>
</head>

<body>

<h1>Add Movie Event</h1>
<?php 
$errors = 0;
$movie_title = "";
$article_title = "";
$article_content = "";
if (empty($_POST['movie_title']) || empty($_POST['article_title']) || empty($_POST['article_content'])) {
	++$errors;
	echo "<p>You need to enter all the require fields.</p>\n";
} else {
	$movie_title = stripslashes($_POST['movie_title']);
	$article_title = stripslashes($_POST['article_title']);
	$article_content = stripslashes($_POST['article_content']);
}
if ($errors == 0) {
	$DBConnect = @mysql_connect("cloudhadoop", "nlp", "nlp_proj");
	if ($DBConnect === FALSE) {
		echo "<p>Unable to connect to the database server. ".
				"Error code: ".mysql_errno().": ".
				mysql_error()."</p>\n";
		++$errors;
	} else {
		$DBName = "nlp";
		$result = @mysql_select_db($DBName, $DBConnect);
		if ($result === FALSE) {
			echo "<p>Unable to select the database. ".
					"Error code ".mysql_errno($DBConnect).
					": ".mysql_error($DBConnect)."</p>\n";
			++$errors;
		}
	}
}
if ($errors > 0) {
	echo "<p>Please use your browser's BACK button to return".
			" to the form and fix the errors indicated.</p>\n";
}
if ($errors == 0) {
	$TableName = "movie_event";
	$movie_title = mysql_real_escape_string($movie_title);
	$article_title = mysql_real_escape_string($article_title);
	$article_content = mysql_real_escape_string($article_content);
	$SQLString = "INSERT INTO $TableName ".
			" (display_title, article_title, article) ".
			" VALUES('$movie_title', '$article_title', '$article_content')";
	$QueryResult = @mysql_query($SQLString, $DBConnect);
	if ($QueryResult === FALSE) {
		echo "<p>Unable to add the movie event ".
				" information. Error code ".
				mysql_errno($DBConnect).": ".
				mysql_error($DBConnect)."</p>\n";
		++$errors;
	}
	mysql_close($DBConnect);
}
if ($errors == 0) {
	echo "<p>The movie $movie_title has been added successfully</p>\n";
	echo "<a href=\"event_upload.php\">Add a new one</a>\n";
}
?>

</body>

</html>
