<?php

function getMovieDetail($search_title) {
	$errors = 0;
	$DBConnect = @mysql_connect("cloudhadoop", "nlp", "nlp_proj");
	if ($DBConnect === FALSE) {
		echo "<p>Unable to connect to the database server.</p>"
				."<p>Error code ".mysql_errno().": ".mysql_error()."</p>";
		++$errors;
	} else {
		$DBName = "nlp";
		$result = @mysql_select_db($DBName, $DBConnect);
		if ($result === FALSE) {
			echo "<p>Unable to select the database. ".
					"Error code ".mysql_errno($DBConnect).": ".
					mysql_error($DBConnect)."</p>\n";
			++$errors;
		}
	}

	$TableName = "movie_review";
	if ($errors == 0) {
		$SQLString = "SELECT * FROM $TableName WHERE display_title = '".mysql_real_escape_string($search_title)."'";
		// 			echo $SQLString."<br>\n";
		$QueryResult = @mysql_query($SQLString, $DBConnect);
		if ($QueryResult === FALSE || mysql_num_rows($QueryResult) == 0) {
			$SQLString = "SELECT * FROM $TableName WHERE display_title like '%".mysql_real_escape_string($search_title)."%' limit 0, 1";
			// 			echo $SQLString."<br>\n";
			$QueryResult = @mysql_query($SQLString, $DBConnect);
			if ($QueryResult === FALSE) {
				echo "<p>Unable to execute the query. ".
						"Error code ".mysql_errno($DBConnect).": ".
						mysql_error($DBConnect)."</p>\n";
				++$errors;
			}
		}
	}
	$movies = null;
	if ($errors == 0) {
		if (($Row = mysql_fetch_assoc($QueryResult)) !== FALSE) {
			$movies = ['source_id'=>$Row['source_id'],
			'movie_id'=>$Row['movie_id'], 'display_title'=>$Row['display_title'],
			'img_url'=>$Row['img_url'], 'overview_url'=>$Row['overview_url'],
			'article_url'=>$Row['article_url'], 'country_list'=>$Row['country_list'],
			'genre_list'=>$Row['genre_list'], 'category_list'=>$Row['category_list'],
			'cast_list'=>$Row['cast_list'], 'role_list'=>$Row['role_list'],
			'article_title'=>$Row['article_title'], 'publication_date'=>$Row['publication_date'],
			'article'=>$Row['article'], 'category_vector'=>$Row['category_vector'],
			];
		}
		mysql_free_result($QueryResult);
	}
	return $movies;
}

function getMovieEventDetail($search_title) {
	$errors = 0;
	$DBConnect = @mysql_connect("cloudhadoop", "nlp", "nlp_proj");
	if ($DBConnect === FALSE) {
		echo "<p>Unable to connect to the database server.</p>"
				."<p>Error code ".mysql_errno().": ".mysql_error()."</p>";
		++$errors;
	} else {
		$DBName = "nlp";
		$result = @mysql_select_db($DBName, $DBConnect);
		if ($result === FALSE) {
			echo "<p>Unable to select the database. ".
					"Error code ".mysql_errno($DBConnect).": ".
					mysql_error($DBConnect)."</p>\n";
			++$errors;
		}
	}

	$TableName = "movie_event";
	if ($errors == 0) {
		$SQLString = "SELECT * FROM $TableName WHERE display_title = '".mysql_real_escape_string($search_title)."'";
		$QueryResult = @mysql_query($SQLString, $DBConnect);
		if ($QueryResult === FALSE) {
			echo "<p>Unable to execute the query. ".
					"Error code ".mysql_errno($DBConnect).": ".
					mysql_error($DBConnect)."</p>\n";
			++$errors;
		}
	}

	$movies = null;
	if ($errors == 0) {
		if (($Row = mysql_fetch_assoc($QueryResult)) !== FALSE) {
			$movies = [
			'movie_id'=>$Row['movie_id'], 'display_title'=>$Row['display_title'],
			'article_title'=>$Row['article_title'],'article'=>$Row['article'],
			'category_vector'=>$Row['category_vector']
			];
		}
		mysql_free_result($QueryResult);
	}
	return $movies;
}

?>