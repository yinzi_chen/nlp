Installation tested on Ubuntu 12.04 
1. Install PHP+Apache
sudo apt-get install apache2
sudo apt-get install php5
sudo apt-get install libapache2-mod-php5
sudo /etc/init.d/apache2 restart
2. Copy the project directory "nlp" into "/var/www/" which is the default web path of the Apache server
sudo cp $path/nlp /var/www
3. Create cache directories with group's read/write permission
sudo mkdir /var/cache
sudo mkdir /var/cache/cinema_location_cache
sudo mkdir /var/cache/movie_events_cache
chmod -R a+wr /var/cache
3. Since the database is always running in the cloud computing lab's server "cloudhadoop", and all the database connection information have already been hardcoded into the php files, you can directly access the demo pages by the following url
http://localhost/nlp/web
